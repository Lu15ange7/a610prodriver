import sys, os, shutil
import configparser
from PySide6.QtWidgets import QApplication, QWidget, QMessageBox

from ui_form import Ui_Widget
#---------------------------------------------------------------------#
#---------------------CHECK IF CONFIG INI EXIST-----------------------#
#---------------------------------------------------------------------#
user_dir = "~/.config/paneltablet/"
name_file = "paneltablet_config.ini"
link_dir = os.path.expanduser(user_dir)
link_file = os.path.join(link_dir, name_file)
link_file_origin = "/opt/a610pro/paneltablet_config.ini"

# check if exist directory
if not os.path.exists(link_dir):
    os.makedirs(link_dir)

# check if exist file
if not os.path.exists(link_file):
    shutil.copyfile(link_file_origin, link_file)
#---------------------------------------------------------------------#
#--------------------------WINDOW FORM LOAD---------------------------#
#---------------------------------------------------------------------#
class Widget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Widget()
        self.ui.setupUi(self)
#---------------------------------------------------------------------#
#-------------------------BUTTONS VARIABLES---------------------------#
#---------------------------------------------------------------------#
        #   Page button
        self.ui.home_page_btn.clicked.connect(lambda: self.ui.stackedWidget.setCurrentWidget(self.ui.home_page))
        self.ui.keys_page_btn.clicked.connect(lambda: self.ui.stackedWidget.setCurrentWidget(self.ui.key_page))
        self.ui.area_page_btn.clicked.connect(lambda: self.ui.stackedWidget.setCurrentWidget(self.ui.area_page))
        self.ui.about_page_btn.clicked.connect(lambda: self.ui.stackedWidget.setCurrentWidget(self.ui.about_page))

        #   Apply button
        self.ui.apply_button.clicked.connect(self.apply_change_save)

        #   Reset button
        self.ui.reset_all_button.clicked.connect(self.reset_button)        #reset all
        self.ui.reset_area_button.clicked.connect(self.reset_area)         #reset area
        self.ui.reset_pressure_button.clicked.connect(self.reset_pressure) #reset pressure
        self.ui.reset_keyex_button.clicked.connect(self.reset_keyex)       #reset keyexpress
        self.ui.reset_rotation_button.clicked.connect(self.reset_rotation) #reset retation
        self.ui.reset_mouse_button.clicked.connect(self.reset_pen_option)  #reset mouse
        self.ui.reset_modepen_button.clicked.connect(self.reset_penmode)   #reset mode pen

        #   Exit button
        self.ui.exit_button.clicked.connect(self.exit_window)

        #   Set main profile
        self.ui.setmainprofile_button.clicked.connect(self.mainprofile_button)

        #   Screenshot View
        self.take_screenshot()

#---------------------------------------------------------------------#
#------------------------LOAD CONFIG INI FILE-------------------------#
#---------------------------------------------------------------------#

    #   load profile
        config = configparser.ConfigParser()
        config.read(link_file)
        profile_ini = config.get('Default_Profile','profile_prede')
        self.ui.profile_combobox.setCurrentText(profile_ini)
        self.ui.profile_combobox.currentIndexChanged.connect(self.update_profilecombo)
        self.update_profilecombo(self)

	#   load area spin default
        self.ui.modearea_combobox.currentIndexChanged.connect(self.update_areamode)
        self.ui.area1_spin.valueChanged.connect(self.update_area)
        self.ui.area2_spin.valueChanged.connect(self.update_area)
        self.ui.area3_spin.valueChanged.connect(self.update_area)
        self.ui.area4_spin.valueChanged.connect(self.update_area)

    #   PressureCurve
        self.ui.pressurecurve_combobox.currentIndexChanged.connect(self.update_pressurecurve_combo)
        self.ui.PC1_SLIDER.valueChanged.connect(self.updateLabel_1)
        self.ui.PC2_SLIDER.valueChanged.connect(self.updateLabel_2)
        self.ui.PC3_SLIDER.valueChanged.connect(self.updateLabel_3)
        self.ui.PC4_SLIDER.valueChanged.connect(self.updateLabel_4)

    #   Screenshoot view
    def take_screenshot(self):
        screen = QApplication.primaryScreen()
        pixmap = screen.grabWindow(0)
        self.ui.screen_pixmap.setPixmap(pixmap)

    #   load  other profile vaules from config ini

    def update_profilecombo(self, value):

        config = configparser.ConfigParser()
        config.read(link_file)

        seccion = 'Default_Profile'
        clave = 'profile_set'
        nuevo_valor = self.ui.profile_combobox.currentText()

        profile_ini_2 = config.set(seccion, clave, nuevo_valor)

        with open(link_file, 'w') as config_file:
            config.write(config_file)

        profile_ini = config.get('Default_Profile','profile_set')

        K1_ini = config.get(profile_ini,'K1')
        K2_ini = config.get(profile_ini,'K2')
        K3_ini = config.get(profile_ini,'K3')
        K4_ini = config.get(profile_ini,'K4')
        KC_ini = config.get(profile_ini,'KC')
        K5_ini = config.get(profile_ini,'K5')
        K6_ini = config.get(profile_ini,'K6')
        K7_ini = config.get(profile_ini,'K7')
        K8_ini = config.get(profile_ini,'K8')
        P1_ini = config.get(profile_ini,'P1')
        P2_ini = config.get(profile_ini,'P2')
        P3_ini = config.get(profile_ini,'P3')
        Area_1_ini = config.get(profile_ini,'Area_1')
        Area_1_ini = int (Area_1_ini)
        Area_2_ini = config.get(profile_ini,'Area_2')
        Area_2_ini = int (Area_2_ini)
        Area_3_ini = config.get(profile_ini,'Area_3')
        Area_3_ini = int (Area_3_ini)
        Area_4_ini = config.get(profile_ini,'Area_4')
        Area_4_ini = int (Area_4_ini)
        PressureCurve_1_ini = config.get(profile_ini,'PressureCurve_1')
        PressureCurve_1_ini = int (PressureCurve_1_ini)
        PressureCurve_2_ini = config.get(profile_ini,'PressureCurve_2')
        PressureCurve_2_ini = int (PressureCurve_2_ini)
        PressureCurve_3_ini = config.get(profile_ini,'PressureCurve_3')
        PressureCurve_3_ini = int (PressureCurve_3_ini)
        PressureCurve_4_ini = config.get(profile_ini,'PressureCurve_4')
        PressureCurve_4_ini = int (PressureCurve_4_ini)
        Mode_ini = config.get(profile_ini,'Mode')
        Rotate_ini = config.get(profile_ini,'Rotate')
        Pressure_combo_ini = config.get(profile_ini,'Pressure_combo')
        area_combo_ini = config.get(profile_ini,'area_combo')

        self.ui.K1_textbox.setText(K1_ini)
        self.ui.K2_textbox.setText(K2_ini)
        self.ui.K3_textbox.setText(K3_ini)
        self.ui.K4_textbox.setText(K4_ini)
        self.ui.KC_textbox.setText(KC_ini)
        self.ui.K5_textbox.setText(K5_ini)
        self.ui.K6_textbox.setText(K6_ini)
        self.ui.K7_textbox.setText(K7_ini)
        self.ui.K8_textbox.setText(K8_ini)
        self.ui.P1_combobox.setCurrentText(P1_ini)
        self.ui.P2_combobox.setCurrentText(P2_ini)
        self.ui.P3_combobox.setCurrentText(P3_ini)
        self.ui.area1_spin.setValue(Area_1_ini)
        self.ui.area2_spin.setValue(Area_2_ini)
        self.ui.area3_spin.setValue(Area_3_ini)
        self.ui.area4_spin.setValue(Area_4_ini)
        self.ui.PC1_SLIDER.setValue(PressureCurve_1_ini)
        self.ui.PC2_SLIDER.setValue(PressureCurve_2_ini)
        self.ui.PC3_SLIDER.setValue(PressureCurve_3_ini)
        self.ui.PC4_SLIDER.setValue(PressureCurve_4_ini)
        self.ui.PC1.setText(str(PressureCurve_1_ini))
        self.ui.PC2.setText(str(PressureCurve_2_ini))
        self.ui.PC3.setText(str(PressureCurve_3_ini))
        self.ui.PC4.setText(str(PressureCurve_4_ini))
        self.ui.modepen_combobox.setCurrentText(Mode_ini)
        self.ui.rotation_combobox.setCurrentText(Rotate_ini)
        self.ui.pressurecurve_combobox.setCurrentText(Pressure_combo_ini)
        self.ui.modearea_combobox.setCurrentText(area_combo_ini)

    #   Main profile autorun
        self.apply_change_save()

    #   Area value change
    def update_area(self, value):
        self.ui.modearea_combobox.setCurrentIndex(2)

    def update_areamode(self, value):
        if self.ui.modearea_combobox.currentIndex() == 0:  #FULL AREA
            self.ui.area1_spin.setValue(0)
            self.ui.area2_spin.setValue(0)
            self.ui.area3_spin.setValue(51094)
            self.ui.area4_spin.setValue(31993)
            self.ui.modearea_combobox.setCurrentIndex(0)
        else:
            if self.ui.modearea_combobox.currentIndex() == 1:  #OSU AREA
                self.ui.area1_spin.setValue(19047)
                self.ui.area2_spin.setValue(12496)
                self.ui.area3_spin.setValue(32047)
                self.ui.area4_spin.setValue(19496)
                self.ui.modearea_combobox.setCurrentIndex(1)

    def update_pressurecurve_combo(self, value):
        if self.ui.pressurecurve_combobox.currentIndex() == 0: #DEFAULT PRESSURECURVE
            self.ui.PC1_SLIDER.setValue(0)
            self.ui.PC2_SLIDER.setValue(0)
            self.ui.PC3_SLIDER.setValue(100)
            self.ui.PC4_SLIDER.setValue(100)
            self.ui.pressurecurve_combobox.setCurrentIndex(0)
        else:
            if self.ui.pressurecurve_combobox.currentIndex() == 1: #SOFT PRESSURECURVE
                self.ui.PC1_SLIDER.setValue(25)
                self.ui.PC2_SLIDER.setValue(50)
                self.ui.PC3_SLIDER.setValue(50)
                self.ui.PC4_SLIDER.setValue(75)
                self.ui.pressurecurve_combobox.setCurrentIndex(1)
            else:
                if self.ui.pressurecurve_combobox.currentIndex() == 2: #HARD PRESSURECURVE
                    self.ui.PC1_SLIDER.setValue(50)
                    self.ui.PC2_SLIDER.setValue(25)
                    self.ui.PC3_SLIDER.setValue(75)
                    self.ui.PC4_SLIDER.setValue(50)
                    self.ui.pressurecurve_combobox.setCurrentIndex(2)
                else:
                    if self.ui.pressurecurve_combobox.currentIndex() == 3: #VERY SOFT PRESSURECURVE
                        self.ui.PC1_SLIDER.setValue(25)
                        self.ui.PC2_SLIDER.setValue(75)
                        self.ui.PC3_SLIDER.setValue(25)
                        self.ui.PC4_SLIDER.setValue(75)
                        self.ui.pressurecurve_combobox.setCurrentIndex(3)
                    else:
                        if self.ui.pressurecurve_combobox.currentIndex() == 4: #VERY HARD PRESSURECURVE
                            self.ui.PC1_SLIDER.setValue(75)
                            self.ui.PC2_SLIDER.setValue(25)
                            self.ui.PC3_SLIDER.setValue(75)
                            self.ui.PC4_SLIDER.setValue(25)
                            self.ui.pressurecurve_combobox.setCurrentIndex(4)
                        else:
                            if self.ui.pressurecurve_combobox.currentIndex() == 5: #NO HIGH PRESSURECURVE
                                self.ui.PC1_SLIDER.setValue(0)
                                self.ui.PC2_SLIDER.setValue(0)
                                self.ui.PC3_SLIDER.setValue(75)
                                self.ui.PC4_SLIDER.setValue(100)
                                self.ui.pressurecurve_combobox.setCurrentIndex(5)
                            else:
                                if self.ui.pressurecurve_combobox.currentIndex() == 6: #FIX LOW PRESSURECURVE
                                    self.ui.PC1_SLIDER.setValue(15)
                                    self.ui.PC2_SLIDER.setValue(0)
                                    self.ui.PC3_SLIDER.setValue(100)
                                    self.ui.PC4_SLIDER.setValue(100)
                                    self.ui.pressurecurve_combobox.setCurrentIndex(6)


    #   Pressure value change
    def updateLabel_1(self, value):
        self.ui.PC1.setText(str(value))
        self.ui.pressurecurve_combobox.setCurrentIndex(7)
    def updateLabel_2(self, value):
        self.ui.PC2.setText(str(value))
        self.ui.pressurecurve_combobox.setCurrentIndex(7)
    def updateLabel_3(self, value):
        self.ui.PC3.setText(str(value))
        self.ui.pressurecurve_combobox.setCurrentIndex(7)
    def updateLabel_4(self, value):
        self.ui.PC4.setText(str(value))
        self.ui.pressurecurve_combobox.setCurrentIndex(7)

    def mainprofile_button(self):
        config = configparser.ConfigParser()
        config.read(link_file)

        seccion = 'Default_Profile'
        clave = 'profile_prede'
        nuevo_valor = self.ui.profile_combobox.currentText()

        profile_ini = config.set(seccion, clave, nuevo_valor)

        with open(link_file, 'w') as config_file:
            config.write(config_file)


#---------------------------------------------------------------------#
#-------------------------APPLY DEF VARIABLES-------------------------#
#---------------------------------------------------------------------#

    #   APPLY CHANGE
    def apply_change_save (self):

        if self.ui.K1_textbox.text() != '' and self.ui.K2_textbox.text() != '' and self.ui.K3_textbox.text() != '' and self.ui.K4_textbox.text() != '' and self.ui.K5_textbox.text() != '' and self.ui.K6_textbox.text() != '' and self.ui.K7_textbox.text() != '' and self.ui.K8_textbox.text() != '' and self.ui.KC_textbox.text() != '':

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "1" "key {self.ui.K1_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K1_writed = 'K1'
            nuevo_valor = self.ui.K1_textbox.text()
            profile_ini = config.set(seccion, K1_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "2" "key {self.ui.K2_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K2_writed = 'K2'
            nuevo_valor = self.ui.K2_textbox.text()
            profile_ini = config.set(seccion, K2_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "3" "key {self.ui.K3_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K3_writed = 'K3'
            nuevo_valor = self.ui.K3_textbox.text()
            profile_ini = config.set(seccion, K3_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "8" "key {self.ui.K4_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K4_writed = 'K4'
            nuevo_valor = self.ui.K4_textbox.text()
            profile_ini = config.set(seccion, K4_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "13" "key {self.ui.KC_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            KC_writed = 'KC'
            nuevo_valor = self.ui.KC_textbox.text()
            profile_ini = config.set(seccion, KC_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "9" "key {self.ui.K5_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K5_writed = 'K5'
            nuevo_valor = self.ui.K5_textbox.text()
            profile_ini = config.set(seccion, K5_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "10" "key {self.ui.K6_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K6_writed = 'K6'
            nuevo_valor = self.ui.K6_textbox.text()
            profile_ini = config.set(seccion, K6_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "11" "key {self.ui.K7_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K7_writed = 'K7'
            nuevo_valor = self.ui.K7_textbox.text()
            profile_ini = config.set(seccion, K7_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "12" "key {self.ui.K8_textbox.text()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            K8_writed = 'K8'
            nuevo_valor = self.ui.K8_textbox.text()
            profile_ini = config.set(seccion, K8_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Button" "3" "{self.ui.P3_combobox.currentText()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            P3_writed = 'P3'
            nuevo_valor = self.ui.P3_combobox.currentText()
            profile_ini = config.set(seccion, P3_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Button" "2" "{self.ui.P2_combobox.currentText()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            P2_writed = 'P2'
            nuevo_valor = self.ui.P2_combobox.currentText()
            profile_ini = config.set(seccion, P2_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Button" "1" "{self.ui.P1_combobox.currentText()} "')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            P1_writed = 'P1'
            nuevo_valor = self.ui.P1_combobox.currentText()
            profile_ini = config.set(seccion, P1_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Mode" "{self.ui.modepen_combobox.currentText()}"')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            mode_writed = 'mode'
            nuevo_valor = self.ui.modepen_combobox.currentText()
            profile_ini = config.set(seccion, mode_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Rotate" "{self.ui.rotation_combobox.currentText()}"')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            rotate_writed = 'rotate'
            nuevo_valor = self.ui.rotation_combobox.currentText()
            profile_ini = config.set(seccion, rotate_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "PressureCurve" "{self.ui.PC1_SLIDER.value()} {self.ui.PC2_SLIDER.value()} {self.ui.PC3_SLIDER.value()} {self.ui.PC4_SLIDER.value()}"')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            PC1_writed = 'pressurecurve_1'
            nuevo_valor = self.ui.PC1.text()
            profile_ini = config.set(seccion, PC1_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            PC2_writed = 'pressurecurve_2'
            nuevo_valor = self.ui.PC2.text()
            profile_ini = config.set(seccion, PC2_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            PC3_writed = 'pressurecurve_3'
            nuevo_valor = self.ui.PC3.text()
            profile_ini = config.set(seccion, PC3_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            PC4_writed = 'pressurecurve_4'
            nuevo_valor = self.ui.PC4.text()
            profile_ini = config.set(seccion, PC4_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Area" "{self.ui.area1_spin.value()} {self.ui.area2_spin.value()} {self.ui.area3_spin.value()} {self.ui.area4_spin.value()}"')
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            area_1_writed = 'area_1'
            nuevo_valor = self.ui.area1_spin.text()
            profile_ini = config.set(seccion, area_1_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            area_2_writed = 'area_2'
            nuevo_valor = self.ui.area2_spin.text()
            profile_ini = config.set(seccion, area_2_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            area_3_writed = 'area_3'
            nuevo_valor = self.ui.area3_spin.text()
            profile_ini = config.set(seccion, area_3_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            area_4_writed = 'area_4'
            nuevo_valor = self.ui.area4_spin.text()
            profile_ini = config.set(seccion, area_4_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)

            #   presure combo
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            pressurecombo_writed = 'pressure_combo'
            nuevo_valor = self.ui.pressurecurve_combobox.currentText()
            profile_ini = config.set(seccion, pressurecombo_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)
            #   area combo
            config = configparser.ConfigParser()
            config.read(link_file)
            seccion = self.ui.profile_combobox.currentText()
            areacombo_writed = 'area_combo'
            nuevo_valor = self.ui.modearea_combobox.currentText()
            profile_ini = config.set(seccion, areacombo_writed, nuevo_valor)
            with open(link_file, 'w') as config_file:
                config.write(config_file)


        else:
            msgBox = QMessageBox()
            msgBox.setText("ERROR: PLEASE, CHECK IF THERE SOME EMPTY DATA")
            msgBox.exec()

#---------------------------------------------------------------------#
#------------------------RESTORE DEF VARIABLES------------------------#
#---------------------------------------------------------------------#
    def reset_keyex(self):
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "1" "key b "')
        self.ui.K1_textbox.setText("b")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "2" "key e "')
        self.ui.K2_textbox.setText("e")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "3" "key ctrl "')
        self.ui.K3_textbox.setText("ctrl")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "8" "key tab "')
        self.ui.K4_textbox.setText("tab")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "13" "key super d "')
        self.ui.KC_textbox.setText("super d")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "9" "key 4 "')
        self.ui.K5_textbox.setText("4")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "10" "key 6 "')
        self.ui.K6_textbox.setText("6")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "11" "key ctrl y "')
        self.ui.K7_textbox.setText("ctrl y")
        os.system(f'xsetwacom set "10 inch PenTablet Pad pad" "Button" "12" "key ctrl z "')
        self.ui.K8_textbox.setText("ctrl z")
    def reset_pen_option(self):
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Button" "3" "button +2 "')
        self.ui.P3_combobox.setCurrentIndex(0)
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Button" "2" "button +3 "')
        self.ui.P2_combobox.setCurrentIndex(0)
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Button" "1" "button +1 "')
        self.ui.P1_combobox.setCurrentIndex(0)
    def reset_penmode(self):
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Mode" "absolute"')
        self.ui.modepen_combobox.setCurrentIndex(0)
    def reset_rotation(self):
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Rotate" "none"')
        self.ui.rotation_combobox.setCurrentIndex(0)
    def reset_pressure(self):
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "PressureCurve" "0 0 100 100"')
        self.ui.PC1_SLIDER.setValue(0)
        self.ui.PC2_SLIDER.setValue(0)
        self.ui.PC3_SLIDER.setValue(100)
        self.ui.PC4_SLIDER.setValue(100)
        self.ui.pressurecurve_combobox.setCurrentIndex(0)
    def reset_area(self):
        os.system(f'xsetwacom set "10 inch PenTablet Pen stylus" "Area" "0 0 32767 32767"')
        self.ui.area1_spin.setValue(0)
        self.ui.area2_spin.setValue(0)
        self.ui.area3_spin.setValue(51094)
        self.ui.area4_spin.setValue(31993)
        self.ui.modearea_combobox.setCurrentIndex(0)

    def reset_button(self):
        self.reset_pen_option()
        self.reset_rotation()
        self.reset_penmode()
        self.reset_pressure()
        self.reset_area()
        self.reset_keyex()
        self.apply_change_save()

#---------------------------------------------------------------------#
#------------------------WINDOW DEF BUTTONS---------------------------#
#---------------------------------------------------------------------#
    #   Exit
    def exit_window(self):
        self.close()

#---------------------------------------------------------------------#
#---------------------------END THE APP-------------------------------#
#---------------------------------------------------------------------#

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = Widget()
    widget.show()
    sys.exit(app.exec())
