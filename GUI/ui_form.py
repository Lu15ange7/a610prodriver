# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 6.7.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QGraphicsView, QHBoxLayout,
    QLabel, QLineEdit, QPlainTextEdit, QPushButton,
    QSizePolicy, QSlider, QSpacerItem, QSpinBox,
    QStackedWidget, QVBoxLayout, QWidget)
import img_rc

class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(880, 600)
        Widget.setMinimumSize(QSize(880, 600))
        Widget.setMaximumSize(QSize(880, 600))
        icon = QIcon()
        icon.addFile(u":/img/img/paneltablet.svg", QSize(), QIcon.Mode.Normal, QIcon.State.Off)
        Widget.setWindowIcon(icon)
        Widget.setStyleSheet(u"")
        self.horizontalLayout = QHBoxLayout(Widget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.menu_bar = QWidget(Widget)
        self.menu_bar.setObjectName(u"menu_bar")
        self.menu_bar.setMaximumSize(QSize(100, 16777215))
        self.menu_bar.setStyleSheet(u"QWidget{\n"
"    background-color: #181825;\n"
"	border-right: 2px solid #a6e3a1;\n"
"}\n"
"\n"
"\n"
"")
        self.verticalLayout = QVBoxLayout(self.menu_bar)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget_2 = QWidget(self.menu_bar)
        self.widget_2.setObjectName(u"widget_2")
        self.widget_2.setStyleSheet(u"QWidget{\n"
"	border-right: 0px;\n"
"}\n"
"\n"
"QPushButton{\n"
"    border-radius:10px;\n"
"    border: 0px;\n"
"}\n"
"QPushButton:hover{\n"
"    border-radius:10px;\n"
"        border-right: 1px;\n"
"    border: 2px solid #a6e3a1;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: #1e1e2e;\n"
"    border-radius:10px;\n"
"    border: 0px;\n"
"}")
        self.verticalLayout_3 = QVBoxLayout(self.widget_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.home_page_btn = QPushButton(self.widget_2)
        self.home_page_btn.setObjectName(u"home_page_btn")
        self.home_page_btn.setMinimumSize(QSize(60, 60))
        icon1 = QIcon()
        icon1.addFile(u":/img/img/home.svg", QSize(), QIcon.Mode.Normal, QIcon.State.Off)
        self.home_page_btn.setIcon(icon1)
        self.home_page_btn.setIconSize(QSize(40, 40))

        self.verticalLayout_3.addWidget(self.home_page_btn)

        self.verticalSpacer_3 = QSpacerItem(20, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Minimum)

        self.verticalLayout_3.addItem(self.verticalSpacer_3)

        self.keys_page_btn = QPushButton(self.widget_2)
        self.keys_page_btn.setObjectName(u"keys_page_btn")
        self.keys_page_btn.setMinimumSize(QSize(60, 60))
        icon2 = QIcon()
        icon2.addFile(u":/img/img/grid.svg", QSize(), QIcon.Mode.Normal, QIcon.State.Off)
        self.keys_page_btn.setIcon(icon2)
        self.keys_page_btn.setIconSize(QSize(40, 40))

        self.verticalLayout_3.addWidget(self.keys_page_btn)

        self.verticalSpacer_5 = QSpacerItem(20, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Minimum)

        self.verticalLayout_3.addItem(self.verticalSpacer_5)

        self.area_page_btn = QPushButton(self.widget_2)
        self.area_page_btn.setObjectName(u"area_page_btn")
        self.area_page_btn.setMinimumSize(QSize(60, 60))
        icon3 = QIcon()
        icon3.addFile(u":/img/img/monitor.svg", QSize(), QIcon.Mode.Normal, QIcon.State.Off)
        self.area_page_btn.setIcon(icon3)
        self.area_page_btn.setIconSize(QSize(40, 40))

        self.verticalLayout_3.addWidget(self.area_page_btn)


        self.verticalLayout.addWidget(self.widget_2)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.widget_3 = QWidget(self.menu_bar)
        self.widget_3.setObjectName(u"widget_3")
        self.widget_3.setStyleSheet(u"QWidget{\n"
"	border-right: 0px;\n"
"}\n"
"\n"
"QPushButton{\n"
"    border-radius:10px;\n"
"    border: 0px;\n"
"}\n"
"QPushButton:hover{\n"
"    border-radius:10px;\n"
"        border-right: 1px;\n"
"    border: 2px solid #a6e3a1;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: #1e1e2e;\n"
"    border-radius:10px;\n"
"    border: 0px;\n"
"}")
        self.verticalLayout_4 = QVBoxLayout(self.widget_3)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.about_page_btn = QPushButton(self.widget_3)
        self.about_page_btn.setObjectName(u"about_page_btn")
        self.about_page_btn.setMinimumSize(QSize(60, 60))
        icon4 = QIcon()
        icon4.addFile(u":/img/img/alert-circle.svg", QSize(), QIcon.Mode.Normal, QIcon.State.Off)
        self.about_page_btn.setIcon(icon4)
        self.about_page_btn.setIconSize(QSize(40, 40))

        self.verticalLayout_4.addWidget(self.about_page_btn)


        self.verticalLayout.addWidget(self.widget_3)


        self.horizontalLayout.addWidget(self.menu_bar)

        self.main_window = QWidget(Widget)
        self.main_window.setObjectName(u"main_window")
        self.main_window.setStyleSheet(u"background-color: rgb(30, 30, 46);\n"
"color: rgb(255, 255, 255);")
        self.verticalLayout_2 = QVBoxLayout(self.main_window)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.stackedWidget = QStackedWidget(self.main_window)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.home_page = QWidget()
        self.home_page.setObjectName(u"home_page")
        self.profile_combobox = QComboBox(self.home_page)
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.addItem("")
        self.profile_combobox.setObjectName(u"profile_combobox")
        self.profile_combobox.setGeometry(QRect(370, 360, 131, 31))
        self.profile_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.profile_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.label_2 = QLabel(self.home_page)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(280, 290, 231, 20))
        font = QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.label_2.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.label = QLabel(self.home_page)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(280, 60, 231, 231))
        self.label.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.label.setPixmap(QPixmap(u":/img/img/paneltablet.svg"))
        self.label.setScaledContents(True)
        self.label_26 = QLabel(self.home_page)
        self.label_26.setObjectName(u"label_26")
        self.label_26.setGeometry(QRect(290, 360, 51, 31))
        self.label_26.setFont(font)
        self.label_26.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.setmainprofile_button = QPushButton(self.home_page)
        self.setmainprofile_button.setObjectName(u"setmainprofile_button")
        self.setmainprofile_button.setGeometry(QRect(290, 420, 211, 37))
        font1 = QFont()
        font1.setPointSize(12)
        self.setmainprofile_button.setFont(font1)
        self.setmainprofile_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.setmainprofile_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.setmainprofile_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.stackedWidget.addWidget(self.home_page)
        self.key_page = QWidget()
        self.key_page.setObjectName(u"key_page")
        self.K2_textbox = QLineEdit(self.key_page)
        self.K2_textbox.setObjectName(u"K2_textbox")
        self.K2_textbox.setEnabled(True)
        self.K2_textbox.setGeometry(QRect(260, 100, 101, 29))
        self.K2_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K2_textbox.setAutoFillBackground(False)
        self.K2_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K2_textbox.setFrame(False)
        self.K2_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.reset_keyex_button = QPushButton(self.key_page)
        self.reset_keyex_button.setObjectName(u"reset_keyex_button")
        self.reset_keyex_button.setEnabled(True)
        self.reset_keyex_button.setGeometry(QRect(30, 30, 31, 31))
        font2 = QFont()
        font2.setPointSize(20)
        self.reset_keyex_button.setFont(font2)
        self.reset_keyex_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_keyex_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_keyex_button.setAcceptDrops(False)
        self.reset_keyex_button.setAutoFillBackground(False)
        self.reset_keyex_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.label_16 = QLabel(self.key_page)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setGeometry(QRect(40, 60, 181, 451))
        self.label_16.setAutoFillBackground(False)
        self.label_16.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"color: rgb(255, 255, 255);\n"
"border: 0px;")
        self.label_16.setPixmap(QPixmap(u":/img/img/button.svg"))
        self.K3_textbox = QLineEdit(self.key_page)
        self.K3_textbox.setObjectName(u"K3_textbox")
        self.K3_textbox.setEnabled(True)
        self.K3_textbox.setGeometry(QRect(260, 140, 101, 29))
        self.K3_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K3_textbox.setAutoFillBackground(False)
        self.K3_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K3_textbox.setFrame(False)
        self.K3_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.K4_textbox = QLineEdit(self.key_page)
        self.K4_textbox.setObjectName(u"K4_textbox")
        self.K4_textbox.setEnabled(True)
        self.K4_textbox.setGeometry(QRect(260, 180, 101, 29))
        self.K4_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K4_textbox.setAutoFillBackground(False)
        self.K4_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K4_textbox.setFrame(False)
        self.K4_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.KC_textbox = QLineEdit(self.key_page)
        self.KC_textbox.setObjectName(u"KC_textbox")
        self.KC_textbox.setEnabled(True)
        self.KC_textbox.setGeometry(QRect(260, 270, 101, 29))
        self.KC_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.KC_textbox.setAutoFillBackground(False)
        self.KC_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.KC_textbox.setFrame(False)
        self.KC_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.K5_textbox = QLineEdit(self.key_page)
        self.K5_textbox.setObjectName(u"K5_textbox")
        self.K5_textbox.setGeometry(QRect(260, 360, 101, 29))
        self.K5_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K5_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K5_textbox.setFrame(False)
        self.K5_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.K1_textbox = QLineEdit(self.key_page)
        self.K1_textbox.setObjectName(u"K1_textbox")
        self.K1_textbox.setEnabled(True)
        self.K1_textbox.setGeometry(QRect(260, 60, 101, 29))
        self.K1_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K1_textbox.setAutoFillBackground(False)
        self.K1_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K1_textbox.setFrame(False)
        self.K1_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.K7_textbox = QLineEdit(self.key_page)
        self.K7_textbox.setObjectName(u"K7_textbox")
        self.K7_textbox.setGeometry(QRect(260, 440, 101, 29))
        self.K7_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K7_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K7_textbox.setFrame(False)
        self.K7_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.K8_textbox = QLineEdit(self.key_page)
        self.K8_textbox.setObjectName(u"K8_textbox")
        self.K8_textbox.setEnabled(True)
        self.K8_textbox.setGeometry(QRect(260, 480, 101, 29))
        self.K8_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K8_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K8_textbox.setFrame(False)
        self.K8_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.K6_textbox = QLineEdit(self.key_page)
        self.K6_textbox.setObjectName(u"K6_textbox")
        self.K6_textbox.setGeometry(QRect(260, 400, 101, 29))
        self.K6_textbox.setCursor(QCursor(Qt.CursorShape.IBeamCursor))
        self.K6_textbox.setStyleSheet(u"QLineEdit{\n"
"	border-radius:10px;\n"
"	border: 2px solid #a6e3a1;\n"
"}")
        self.K6_textbox.setFrame(False)
        self.K6_textbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.PC2_SLIDER = QSlider(self.key_page)
        self.PC2_SLIDER.setObjectName(u"PC2_SLIDER")
        self.PC2_SLIDER.setGeometry(QRect(480, 260, 21, 141))
        self.PC2_SLIDER.setCursor(QCursor(Qt.CursorShape.SplitHCursor))
        self.PC2_SLIDER.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.PC2_SLIDER.setStyleSheet(u"QSlider::groove {\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:5px;\n"
"    width: 10px;\n"
"    margin: 0px;\n"
"}\n"
"QSlider::handle {\n"
"    background: #1e1e2e;\n"
"    border-radius:7px;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    margin: -2px;\n"
"    border: 2px solid #a6e3a1;\n"
"}\n"
"\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background-color: #a6e3a1;\n"
"    border-radius:5px;\n"
"    border: 0px;\n"
"}\n"
"")
        self.PC2_SLIDER.setMaximum(100)
        self.PC2_SLIDER.setOrientation(Qt.Orientation.Vertical)
        self.P1_combobox = QComboBox(self.key_page)
        self.P1_combobox.addItem("")
        self.P1_combobox.setObjectName(u"P1_combobox")
        self.P1_combobox.setEnabled(True)
        self.P1_combobox.setGeometry(QRect(620, 120, 121, 31))
        self.P1_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.P1_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.P1_combobox.setFrame(False)
        self.pressurecurve_graphicsview = QGraphicsView(self.key_page)
        self.pressurecurve_graphicsview.setObjectName(u"pressurecurve_graphicsview")
        self.pressurecurve_graphicsview.setGeometry(QRect(510, 270, 121, 121))
        self.pressurecurve_graphicsview.viewport().setProperty("cursor", QCursor(Qt.CursorShape.CrossCursor))
        self.pressurecurve_graphicsview.setStyleSheet(u"border-radius:10px;\n"
"border: 2px solid #a6e3a1;")
        self.pressurecurve_combobox = QComboBox(self.key_page)
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.addItem("")
        self.pressurecurve_combobox.setObjectName(u"pressurecurve_combobox")
        self.pressurecurve_combobox.setGeometry(QRect(620, 480, 121, 31))
        self.pressurecurve_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.pressurecurve_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.label_6 = QLabel(self.key_page)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(470, 480, 103, 31))
        self.label_6.setFont(font)
        self.label_6.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.PC3 = QLabel(self.key_page)
        self.PC3.setObjectName(u"PC3")
        self.PC3.setGeometry(QRect(580, 430, 21, 31))
        self.PC3.setLayoutDirection(Qt.LayoutDirection.LeftToRight)
        self.PC3.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.PC3.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.P2_combobox = QComboBox(self.key_page)
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.addItem("")
        self.P2_combobox.setObjectName(u"P2_combobox")
        self.P2_combobox.setGeometry(QRect(620, 80, 121, 31))
        self.P2_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.P2_combobox.setFocusPolicy(Qt.FocusPolicy.StrongFocus)
        self.P2_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.P2_combobox.setFrame(False)
        self.P3_combobox = QComboBox(self.key_page)
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.addItem("")
        self.P3_combobox.setObjectName(u"P3_combobox")
        self.P3_combobox.setGeometry(QRect(620, 40, 121, 31))
        self.P3_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.P3_combobox.setFocusPolicy(Qt.FocusPolicy.StrongFocus)
        self.P3_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.P3_combobox.setFrame(False)
        self.reset_mouse_button = QPushButton(self.key_page)
        self.reset_mouse_button.setObjectName(u"reset_mouse_button")
        self.reset_mouse_button.setEnabled(True)
        self.reset_mouse_button.setGeometry(QRect(410, 30, 31, 31))
        self.reset_mouse_button.setFont(font2)
        self.reset_mouse_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_mouse_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_mouse_button.setAcceptDrops(False)
        self.reset_mouse_button.setAutoFillBackground(False)
        self.reset_mouse_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.modepen_combobox = QComboBox(self.key_page)
        self.modepen_combobox.addItem("")
        self.modepen_combobox.addItem("")
        self.modepen_combobox.setObjectName(u"modepen_combobox")
        self.modepen_combobox.setGeometry(QRect(620, 190, 121, 31))
        self.modepen_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.modepen_combobox.setAcceptDrops(False)
        self.modepen_combobox.setAutoFillBackground(False)
        self.modepen_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.modepen_combobox.setInsertPolicy(QComboBox.InsertPolicy.NoInsert)
        self.modepen_combobox.setDuplicatesEnabled(False)
        self.modepen_combobox.setFrame(False)
        self.PC4_SLIDER = QSlider(self.key_page)
        self.PC4_SLIDER.setObjectName(u"PC4_SLIDER")
        self.PC4_SLIDER.setGeometry(QRect(640, 260, 21, 141))
        self.PC4_SLIDER.setCursor(QCursor(Qt.CursorShape.SplitHCursor))
        self.PC4_SLIDER.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.PC4_SLIDER.setStyleSheet(u"QSlider::groove {\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:5px;\n"
"    width: 10px;\n"
"    margin: 0px;\n"
"}\n"
"QSlider::handle {\n"
"    background: #1e1e2e;\n"
"    border-radius:7px;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    margin: -2px;\n"
"    border: 2px solid #a6e3a1;\n"
"}\n"
"\n"
"\n"
"QSlider::add-page:vertical {\n"
"    background-color: #a6e3a1;\n"
"    border-radius:5px;\n"
"    border: 0px;\n"
"}\n"
"")
        self.PC4_SLIDER.setMaximum(100)
        self.PC4_SLIDER.setValue(100)
        self.PC4_SLIDER.setOrientation(Qt.Orientation.Vertical)
        self.reset_modepen_button = QPushButton(self.key_page)
        self.reset_modepen_button.setObjectName(u"reset_modepen_button")
        self.reset_modepen_button.setEnabled(True)
        self.reset_modepen_button.setGeometry(QRect(410, 190, 31, 31))
        self.reset_modepen_button.setFont(font2)
        self.reset_modepen_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_modepen_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_modepen_button.setAcceptDrops(False)
        self.reset_modepen_button.setAutoFillBackground(False)
        self.reset_modepen_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.label_5 = QLabel(self.key_page)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(480, 40, 101, 141))
        self.label_5.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.label_5.setPixmap(QPixmap(u":/img/img/pen.svg"))
        self.label_7 = QLabel(self.key_page)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(470, 190, 68, 31))
        self.label_7.setFont(font)
        self.label_7.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.reset_pressure_button = QPushButton(self.key_page)
        self.reset_pressure_button.setObjectName(u"reset_pressure_button")
        self.reset_pressure_button.setEnabled(True)
        self.reset_pressure_button.setGeometry(QRect(410, 480, 31, 31))
        self.reset_pressure_button.setFont(font2)
        self.reset_pressure_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_pressure_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_pressure_button.setAcceptDrops(False)
        self.reset_pressure_button.setAutoFillBackground(False)
        self.reset_pressure_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.PC4 = QLabel(self.key_page)
        self.PC4.setObjectName(u"PC4")
        self.PC4.setGeometry(QRect(620, 430, 21, 31))
        self.PC4.setLayoutDirection(Qt.LayoutDirection.LeftToRight)
        self.PC4.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.PC4.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.PC2 = QLabel(self.key_page)
        self.PC2.setObjectName(u"PC2")
        self.PC2.setGeometry(QRect(540, 430, 21, 31))
        self.PC2.setLayoutDirection(Qt.LayoutDirection.LeftToRight)
        self.PC2.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.PC2.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.PC1 = QLabel(self.key_page)
        self.PC1.setObjectName(u"PC1")
        self.PC1.setGeometry(QRect(500, 430, 21, 31))
        self.PC1.setLayoutDirection(Qt.LayoutDirection.LeftToRight)
        self.PC1.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.PC1.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.PC3_SLIDER = QSlider(self.key_page)
        self.PC3_SLIDER.setObjectName(u"PC3_SLIDER")
        self.PC3_SLIDER.setGeometry(QRect(500, 240, 141, 21))
        self.PC3_SLIDER.setCursor(QCursor(Qt.CursorShape.SplitHCursor))
        self.PC3_SLIDER.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.PC3_SLIDER.setAcceptDrops(False)
        self.PC3_SLIDER.setAutoFillBackground(False)
        self.PC3_SLIDER.setStyleSheet(u"QSlider::groove {\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:5px;\n"
"    height: 10px;\n"
"    margin: 0px;\n"
"}\n"
"QSlider::handle {\n"
"    background: #1e1e2e;\n"
"    border-radius:7px;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    margin: -2px;\n"
"	border: 2px solid #a6e3a1;\n"
"}\n"
"\n"
"\n"
"QSlider::sub-page:horizontal {\n"
"    background-color: #a6e3a1;\n"
"    border-radius:5px;\n"
"    border: 0px;\n"
"}")
        self.PC3_SLIDER.setMaximum(100)
        self.PC3_SLIDER.setValue(100)
        self.PC3_SLIDER.setSliderPosition(100)
        self.PC3_SLIDER.setOrientation(Qt.Orientation.Horizontal)
        self.PC1_SLIDER = QSlider(self.key_page)
        self.PC1_SLIDER.setObjectName(u"PC1_SLIDER")
        self.PC1_SLIDER.setGeometry(QRect(500, 400, 141, 20))
        self.PC1_SLIDER.setCursor(QCursor(Qt.CursorShape.SplitHCursor))
        self.PC1_SLIDER.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.PC1_SLIDER.setToolTipDuration(-1)
        self.PC1_SLIDER.setStyleSheet(u"QSlider::groove {\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:5px;\n"
"    height: 10px;\n"
"    margin: 0px;\n"
"}\n"
"QSlider::handle {\n"
"    background: #1e1e2e;\n"
"    border-radius:7px;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    margin: -2px;\n"
"	border: 2px solid #a6e3a1;\n"
"}\n"
"\n"
"\n"
"QSlider::sub-page:horizontal {\n"
"    background-color: #a6e3a1;\n"
"    border-radius:5px;\n"
"    border: 0px;\n"
"}")
        self.PC1_SLIDER.setMaximum(100)
        self.PC1_SLIDER.setValue(0)
        self.PC1_SLIDER.setSliderPosition(0)
        self.PC1_SLIDER.setTracking(True)
        self.PC1_SLIDER.setOrientation(Qt.Orientation.Horizontal)
        self.stackedWidget.addWidget(self.key_page)
        self.area_page = QWidget()
        self.area_page.setObjectName(u"area_page")
        self.rotation_combobox = QComboBox(self.area_page)
        self.rotation_combobox.addItem("")
        self.rotation_combobox.addItem("")
        self.rotation_combobox.addItem("")
        self.rotation_combobox.addItem("")
        self.rotation_combobox.setObjectName(u"rotation_combobox")
        self.rotation_combobox.setGeometry(QRect(420, 390, 111, 31))
        self.rotation_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.rotation_combobox.setContextMenuPolicy(Qt.ContextMenuPolicy.NoContextMenu)
        self.rotation_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 1px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    border-radius:10px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.rotation_combobox.setInsertPolicy(QComboBox.InsertPolicy.NoInsert)
        self.rotation_combobox.setDuplicatesEnabled(False)
        self.rotation_combobox.setFrame(False)
        self.modearea_combobox = QComboBox(self.area_page)
        self.modearea_combobox.addItem("")
        self.modearea_combobox.addItem("")
        self.modearea_combobox.addItem("")
        self.modearea_combobox.setObjectName(u"modearea_combobox")
        self.modearea_combobox.setGeometry(QRect(420, 350, 111, 31))
        self.modearea_combobox.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.modearea_combobox.setAcceptDrops(False)
        self.modearea_combobox.setAutoFillBackground(False)
        self.modearea_combobox.setStyleSheet(u"QComboBox{\n"
"    margin: 1px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QComboBox QAbstractItemView {\n"
"    border-radius:10px;\n"
"    border: 2px solid #a6e3a1;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"")
        self.modearea_combobox.setInsertPolicy(QComboBox.InsertPolicy.NoInsert)
        self.modearea_combobox.setDuplicatesEnabled(False)
        self.modearea_combobox.setFrame(False)
        self.reset_rotation_button = QPushButton(self.area_page)
        self.reset_rotation_button.setObjectName(u"reset_rotation_button")
        self.reset_rotation_button.setEnabled(True)
        self.reset_rotation_button.setGeometry(QRect(250, 390, 31, 31))
        self.reset_rotation_button.setFont(font2)
        self.reset_rotation_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_rotation_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_rotation_button.setAcceptDrops(False)
        self.reset_rotation_button.setAutoFillBackground(False)
        self.reset_rotation_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.label_22 = QLabel(self.area_page)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setGeometry(QRect(240, 470, 21, 31))
        self.label_22.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.area4_spin = QSpinBox(self.area_page)
        self.area4_spin.setObjectName(u"area4_spin")
        self.area4_spin.setGeometry(QRect(610, 470, 98, 31))
        font3 = QFont()
        font3.setFamilies([u"DejaVu Sans"])
        font3.setPointSize(8)
        self.area4_spin.setFont(font3)
        self.area4_spin.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.area4_spin.setStyleSheet(u"QSpinBox {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QSpinBox:up-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: right;\n"
"}\n"
"QSpinBox:down-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: left;\n"
"}")
        self.area4_spin.setMinimum(0)
        self.area4_spin.setMaximum(99999)
        self.label_15 = QLabel(self.area_page)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setGeometry(QRect(300, 350, 41, 31))
        self.label_15.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.label_14 = QLabel(self.area_page)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setGeometry(QRect(300, 390, 59, 31))
        self.label_14.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);border: 0px;")
        self.area2_spin = QSpinBox(self.area_page)
        self.area2_spin.setObjectName(u"area2_spin")
        self.area2_spin.setGeometry(QRect(450, 470, 98, 31))
        self.area2_spin.setFont(font3)
        self.area2_spin.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.area2_spin.setStyleSheet(u"QSpinBox {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QSpinBox:up-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: right;\n"
"}\n"
"QSpinBox:down-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: left;\n"
"}")
        self.area2_spin.setMinimum(0)
        self.area2_spin.setMaximum(99999)
        self.area2_spin.setValue(0)
        self.area1_spin = QSpinBox(self.area_page)
        self.area1_spin.setObjectName(u"area1_spin")
        self.area1_spin.setGeometry(QRect(120, 470, 98, 31))
        self.area1_spin.setFont(font3)
        self.area1_spin.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.area1_spin.setStyleSheet(u"QSpinBox {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QSpinBox:up-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: right;\n"
"}\n"
"QSpinBox:down-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: left;\n"
"}")
        self.area1_spin.setMinimum(0)
        self.area1_spin.setMaximum(99999)
        self.area1_spin.setValue(0)
        self.reset_area_button = QPushButton(self.area_page)
        self.reset_area_button.setObjectName(u"reset_area_button")
        self.reset_area_button.setEnabled(True)
        self.reset_area_button.setGeometry(QRect(250, 350, 31, 31))
        self.reset_area_button.setFont(font2)
        self.reset_area_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_area_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_area_button.setAcceptDrops(False)
        self.reset_area_button.setAutoFillBackground(False)
        self.reset_area_button.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.label_21 = QLabel(self.area_page)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setGeometry(QRect(80, 470, 21, 31))
        self.label_21.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.area3_spin = QSpinBox(self.area_page)
        self.area3_spin.setObjectName(u"area3_spin")
        self.area3_spin.setGeometry(QRect(280, 470, 98, 31))
        self.area3_spin.setFont(font3)
        self.area3_spin.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.area3_spin.setStyleSheet(u"QSpinBox {\n"
"    margin: 2px;\n"
"    border: 2px solid #a6e3a1;\n"
"    border-radius:10px;\n"
"}\n"
"QSpinBox:up-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: right;\n"
"}\n"
"QSpinBox:down-button {\n"
"    width:22px;\n"
"    height:22px;\n"
"    subcontrol-position: left;\n"
"}")
        self.area3_spin.setMinimum(0)
        self.area3_spin.setMaximum(99999)
        self.label_23 = QLabel(self.area_page)
        self.label_23.setObjectName(u"label_23")
        self.label_23.setGeometry(QRect(410, 470, 21, 31))
        self.label_23.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.label_24 = QLabel(self.area_page)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setGeometry(QRect(570, 470, 21, 31))
        self.label_24.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.widget_4 = QWidget(self.area_page)
        self.widget_4.setObjectName(u"widget_4")
        self.widget_4.setGeometry(QRect(160, 40, 461, 281))
        self.widget_4.setStyleSheet(u"border-radius:10px;\n"
"border: 2px solid #a6e3a1;")
        self.verticalLayout_5 = QVBoxLayout(self.widget_4)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.screen_pixmap = QLabel(self.widget_4)
        self.screen_pixmap.setObjectName(u"screen_pixmap")
        self.screen_pixmap.setStyleSheet(u"border-radius:10px;\n"
"border: 0px solid #a6e3a1;")
        self.screen_pixmap.setTextFormat(Qt.TextFormat.PlainText)
        self.screen_pixmap.setScaledContents(True)
        self.screen_pixmap.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.screen_pixmap.setWordWrap(False)

        self.verticalLayout_5.addWidget(self.screen_pixmap, 0, Qt.AlignmentFlag.AlignHCenter)

        self.stackedWidget.addWidget(self.area_page)
        self.about_page = QWidget()
        self.about_page.setObjectName(u"about_page")
        self.label_19 = QLabel(self.about_page)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setGeometry(QRect(320, 20, 141, 91))
        self.label_19.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.label_19.setPixmap(QPixmap(u":/img/img/peekaboo.png"))
        self.label_19.setScaledContents(True)
        self.label_19.setWordWrap(False)
        self.label_17 = QLabel(self.about_page)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setGeometry(QRect(240, 110, 311, 51))
        self.label_17.setStyleSheet(u"background-color: rgba(255, 255, 255, 0);\n"
"border: 0px;")
        self.plainTextEdit = QPlainTextEdit(self.about_page)
        self.plainTextEdit.setObjectName(u"plainTextEdit")
        self.plainTextEdit.setGeometry(QRect(210, 170, 371, 341))
        self.plainTextEdit.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.plainTextEdit.setStyleSheet(u"color: white;\n"
"border-radius:10px;\n"
"border: 2px solid #a6e3a1;\n"
"\n"
"")
        self.plainTextEdit.setTabChangesFocus(False)
        self.stackedWidget.addWidget(self.about_page)

        self.verticalLayout_2.addWidget(self.stackedWidget)

        self.widget = QWidget(self.main_window)
        self.widget.setObjectName(u"widget")
        self.widget.setMinimumSize(QSize(0, 60))
        self.widget.setStyleSheet(u"QPushButton{\n"
"    background-color: #a6e3a1;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:hover{\n"
"    background-color: #40a02b;\n"
"    border-radius:10px;\n"
"    color:black;\n"
"}\n"
"QPushButton:pressed{\n"
"    background-color: rgb(23, 23, 36);\n"
"    border-radius:10px;\n"
"    color:white;\n"
"}")
        self.horizontalLayout_2 = QHBoxLayout(self.widget)
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.apply_button = QPushButton(self.widget)
        self.apply_button.setObjectName(u"apply_button")
        self.apply_button.setEnabled(True)
        self.apply_button.setMinimumSize(QSize(80, 30))
        self.apply_button.setFont(font1)
        self.apply_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.apply_button.setMouseTracking(True)
        self.apply_button.setTabletTracking(False)
        self.apply_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.apply_button.setAcceptDrops(False)
        self.apply_button.setAutoFillBackground(False)
        self.apply_button.setStyleSheet(u"")
        self.apply_button.setCheckable(False)
        self.apply_button.setChecked(False)
        self.apply_button.setAutoDefault(False)
        self.apply_button.setFlat(False)

        self.horizontalLayout_2.addWidget(self.apply_button, 0, Qt.AlignmentFlag.AlignTop)

        self.horizontalSpacer_2 = QSpacerItem(20, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)

        self.reset_all_button = QPushButton(self.widget)
        self.reset_all_button.setObjectName(u"reset_all_button")
        self.reset_all_button.setEnabled(True)
        self.reset_all_button.setMinimumSize(QSize(80, 30))
        self.reset_all_button.setFont(font1)
        self.reset_all_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.reset_all_button.setMouseTracking(True)
        self.reset_all_button.setTabletTracking(False)
        self.reset_all_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.reset_all_button.setAcceptDrops(False)
        self.reset_all_button.setAutoFillBackground(False)
        self.reset_all_button.setStyleSheet(u"")
        self.reset_all_button.setCheckable(False)
        self.reset_all_button.setChecked(False)
        self.reset_all_button.setAutoDefault(False)
        self.reset_all_button.setFlat(False)

        self.horizontalLayout_2.addWidget(self.reset_all_button, 0, Qt.AlignmentFlag.AlignTop)

        self.horizontalSpacer_3 = QSpacerItem(20, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_3)

        self.exit_button = QPushButton(self.widget)
        self.exit_button.setObjectName(u"exit_button")
        self.exit_button.setEnabled(True)
        self.exit_button.setMinimumSize(QSize(80, 30))
        self.exit_button.setFont(font1)
        self.exit_button.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))
        self.exit_button.setMouseTracking(True)
        self.exit_button.setFocusPolicy(Qt.FocusPolicy.NoFocus)
        self.exit_button.setAcceptDrops(False)
        self.exit_button.setAutoFillBackground(False)
        self.exit_button.setStyleSheet(u"")

        self.horizontalLayout_2.addWidget(self.exit_button, 0, Qt.AlignmentFlag.AlignTop)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_4)


        self.verticalLayout_2.addWidget(self.widget)


        self.horizontalLayout.addWidget(self.main_window)

        QWidget.setTabOrder(self.home_page_btn, self.keys_page_btn)
        QWidget.setTabOrder(self.keys_page_btn, self.area_page_btn)
        QWidget.setTabOrder(self.area_page_btn, self.about_page_btn)
        QWidget.setTabOrder(self.about_page_btn, self.profile_combobox)
        QWidget.setTabOrder(self.profile_combobox, self.K1_textbox)
        QWidget.setTabOrder(self.K1_textbox, self.K2_textbox)
        QWidget.setTabOrder(self.K2_textbox, self.K3_textbox)
        QWidget.setTabOrder(self.K3_textbox, self.K4_textbox)
        QWidget.setTabOrder(self.K4_textbox, self.KC_textbox)
        QWidget.setTabOrder(self.KC_textbox, self.K5_textbox)
        QWidget.setTabOrder(self.K5_textbox, self.K6_textbox)
        QWidget.setTabOrder(self.K6_textbox, self.K7_textbox)
        QWidget.setTabOrder(self.K7_textbox, self.K8_textbox)
        QWidget.setTabOrder(self.K8_textbox, self.P3_combobox)
        QWidget.setTabOrder(self.P3_combobox, self.P2_combobox)
        QWidget.setTabOrder(self.P2_combobox, self.P1_combobox)
        QWidget.setTabOrder(self.P1_combobox, self.modepen_combobox)
        QWidget.setTabOrder(self.modepen_combobox, self.pressurecurve_graphicsview)
        QWidget.setTabOrder(self.pressurecurve_graphicsview, self.pressurecurve_combobox)
        QWidget.setTabOrder(self.pressurecurve_combobox, self.modearea_combobox)
        QWidget.setTabOrder(self.modearea_combobox, self.rotation_combobox)
        QWidget.setTabOrder(self.rotation_combobox, self.area1_spin)
        QWidget.setTabOrder(self.area1_spin, self.area3_spin)
        QWidget.setTabOrder(self.area3_spin, self.area2_spin)
        QWidget.setTabOrder(self.area2_spin, self.area4_spin)

        self.retranslateUi(Widget)

        self.stackedWidget.setCurrentIndex(0)
        self.apply_button.setDefault(False)
        self.reset_all_button.setDefault(False)


        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Panel Tablet", None))
#if QT_CONFIG(tooltip)
        self.home_page_btn.setToolTip(QCoreApplication.translate("Widget", u"Home", None))
#endif // QT_CONFIG(tooltip)
        self.home_page_btn.setText("")
#if QT_CONFIG(tooltip)
        self.keys_page_btn.setToolTip(QCoreApplication.translate("Widget", u"Buttons", None))
#endif // QT_CONFIG(tooltip)
        self.keys_page_btn.setText("")
#if QT_CONFIG(tooltip)
        self.area_page_btn.setToolTip(QCoreApplication.translate("Widget", u"Area", None))
#endif // QT_CONFIG(tooltip)
        self.area_page_btn.setText("")
#if QT_CONFIG(tooltip)
        self.about_page_btn.setToolTip(QCoreApplication.translate("Widget", u"About", None))
#endif // QT_CONFIG(tooltip)
        self.about_page_btn.setText("")
        self.profile_combobox.setItemText(0, QCoreApplication.translate("Widget", u"Default", None))
        self.profile_combobox.setItemText(1, QCoreApplication.translate("Widget", u"Krita", None))
        self.profile_combobox.setItemText(2, QCoreApplication.translate("Widget", u"Gimp", None))
        self.profile_combobox.setItemText(3, QCoreApplication.translate("Widget", u"FireAlpaca", None))
        self.profile_combobox.setItemText(4, QCoreApplication.translate("Widget", u"Inkscape", None))
        self.profile_combobox.setItemText(5, QCoreApplication.translate("Widget", u"OpenToonz", None))
        self.profile_combobox.setItemText(6, QCoreApplication.translate("Widget", u"Synfig", None))
        self.profile_combobox.setItemText(7, QCoreApplication.translate("Widget", u"Blender", None))
        self.profile_combobox.setItemText(8, QCoreApplication.translate("Widget", u"Osu", None))
        self.profile_combobox.setItemText(9, QCoreApplication.translate("Widget", u"Photopea", None))
        self.profile_combobox.setItemText(10, QCoreApplication.translate("Widget", u"Xournal++", None))
        self.profile_combobox.setItemText(11, QCoreApplication.translate("Widget", u"Animator Paper", None))
        self.profile_combobox.setItemText(12, QCoreApplication.translate("Widget", u"Profile custom 1", None))
        self.profile_combobox.setItemText(13, QCoreApplication.translate("Widget", u"Profile custom 2", None))
        self.profile_combobox.setItemText(14, QCoreApplication.translate("Widget", u"Profile custom 3", None))

        self.label_2.setText(QCoreApplication.translate("Widget", u"<html><head/><body><p><span style=\" font-family:'Sans'; font-size:12pt;\">PanelTablet (A610ProDriver)</span></p></body></html>", None))
        self.label.setText("")
        self.label_26.setText(QCoreApplication.translate("Widget", u"Profile:", None))
        self.setmainprofile_button.setText(QCoreApplication.translate("Widget", u"Set Main Profile", None))
        self.K2_textbox.setText(QCoreApplication.translate("Widget", u"e", None))
#if QT_CONFIG(tooltip)
        self.reset_keyex_button.setToolTip(QCoreApplication.translate("Widget", u"reset key express", None))
#endif // QT_CONFIG(tooltip)
        self.reset_keyex_button.setText(QCoreApplication.translate("Widget", u"\u21b6", None))
        self.label_16.setText("")
        self.K3_textbox.setText(QCoreApplication.translate("Widget", u"ctrl", None))
        self.K4_textbox.setText(QCoreApplication.translate("Widget", u"tab", None))
        self.KC_textbox.setText(QCoreApplication.translate("Widget", u"super d", None))
        self.K5_textbox.setText(QCoreApplication.translate("Widget", u"4", None))
        self.K1_textbox.setText(QCoreApplication.translate("Widget", u"b", None))
        self.K7_textbox.setText(QCoreApplication.translate("Widget", u"ctrl y", None))
        self.K8_textbox.setText(QCoreApplication.translate("Widget", u"ctrl z", None))
        self.K6_textbox.setText(QCoreApplication.translate("Widget", u"6", None))
        self.P1_combobox.setItemText(0, QCoreApplication.translate("Widget", u"button +1", None))

        self.pressurecurve_combobox.setItemText(0, QCoreApplication.translate("Widget", u"default", None))
        self.pressurecurve_combobox.setItemText(1, QCoreApplication.translate("Widget", u"soft", None))
        self.pressurecurve_combobox.setItemText(2, QCoreApplication.translate("Widget", u"hard", None))
        self.pressurecurve_combobox.setItemText(3, QCoreApplication.translate("Widget", u"very soft", None))
        self.pressurecurve_combobox.setItemText(4, QCoreApplication.translate("Widget", u"very hard", None))
        self.pressurecurve_combobox.setItemText(5, QCoreApplication.translate("Widget", u"no high pressure", None))
        self.pressurecurve_combobox.setItemText(6, QCoreApplication.translate("Widget", u"fix low pressure", None))
        self.pressurecurve_combobox.setItemText(7, QCoreApplication.translate("Widget", u"custom", None))

        self.label_6.setText(QCoreApplication.translate("Widget", u"Pressure Curve:", None))
        self.PC3.setText(QCoreApplication.translate("Widget", u"100", None))
        self.P2_combobox.setItemText(0, QCoreApplication.translate("Widget", u"button +2", None))
        self.P2_combobox.setItemText(1, QCoreApplication.translate("Widget", u"button +3", None))
        self.P2_combobox.setItemText(2, QCoreApplication.translate("Widget", u"key b", None))
        self.P2_combobox.setItemText(3, QCoreApplication.translate("Widget", u"key e", None))
        self.P2_combobox.setItemText(4, QCoreApplication.translate("Widget", u"key ctrl", None))
        self.P2_combobox.setItemText(5, QCoreApplication.translate("Widget", u"key shift", None))
        self.P2_combobox.setItemText(6, QCoreApplication.translate("Widget", u"key alt", None))
        self.P2_combobox.setItemText(7, QCoreApplication.translate("Widget", u"key tab", None))
        self.P2_combobox.setItemText(8, QCoreApplication.translate("Widget", u"key fn", None))
        self.P2_combobox.setItemText(9, QCoreApplication.translate("Widget", u"key tab", None))
        self.P2_combobox.setItemText(10, QCoreApplication.translate("Widget", u"key super", None))

        self.P3_combobox.setItemText(0, QCoreApplication.translate("Widget", u"button +3", None))
        self.P3_combobox.setItemText(1, QCoreApplication.translate("Widget", u"button +2", None))
        self.P3_combobox.setItemText(2, QCoreApplication.translate("Widget", u"key b", None))
        self.P3_combobox.setItemText(3, QCoreApplication.translate("Widget", u"key e", None))
        self.P3_combobox.setItemText(4, QCoreApplication.translate("Widget", u"key ctrl", None))
        self.P3_combobox.setItemText(5, QCoreApplication.translate("Widget", u"key shift", None))
        self.P3_combobox.setItemText(6, QCoreApplication.translate("Widget", u"key alt", None))
        self.P3_combobox.setItemText(7, QCoreApplication.translate("Widget", u"key tab", None))
        self.P3_combobox.setItemText(8, QCoreApplication.translate("Widget", u"key fn", None))
        self.P3_combobox.setItemText(9, QCoreApplication.translate("Widget", u"key tab", None))
        self.P3_combobox.setItemText(10, QCoreApplication.translate("Widget", u"key super", None))

#if QT_CONFIG(tooltip)
        self.reset_mouse_button.setToolTip(QCoreApplication.translate("Widget", u"reset mouse options", None))
#endif // QT_CONFIG(tooltip)
        self.reset_mouse_button.setText(QCoreApplication.translate("Widget", u"\u21b6", None))
        self.modepen_combobox.setItemText(0, QCoreApplication.translate("Widget", u"absolute", None))
        self.modepen_combobox.setItemText(1, QCoreApplication.translate("Widget", u"relative", None))

#if QT_CONFIG(tooltip)
        self.reset_modepen_button.setToolTip(QCoreApplication.translate("Widget", u"reset mode pen", None))
#endif // QT_CONFIG(tooltip)
        self.reset_modepen_button.setText(QCoreApplication.translate("Widget", u"\u21b6", None))
        self.label_5.setText("")
        self.label_7.setText(QCoreApplication.translate("Widget", u"Mode Pen:", None))
#if QT_CONFIG(tooltip)
        self.reset_pressure_button.setToolTip(QCoreApplication.translate("Widget", u"reset pressure", None))
#endif // QT_CONFIG(tooltip)
        self.reset_pressure_button.setText(QCoreApplication.translate("Widget", u"\u21b6", None))
        self.PC4.setText(QCoreApplication.translate("Widget", u"100", None))
        self.PC2.setText(QCoreApplication.translate("Widget", u"0", None))
        self.PC1.setText(QCoreApplication.translate("Widget", u"0", None))
#if QT_CONFIG(tooltip)
        self.PC1_SLIDER.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.rotation_combobox.setItemText(0, QCoreApplication.translate("Widget", u"none", None))
        self.rotation_combobox.setItemText(1, QCoreApplication.translate("Widget", u"half", None))
        self.rotation_combobox.setItemText(2, QCoreApplication.translate("Widget", u"cw", None))
        self.rotation_combobox.setItemText(3, QCoreApplication.translate("Widget", u"ccw", None))

        self.modearea_combobox.setItemText(0, QCoreApplication.translate("Widget", u"full", None))
        self.modearea_combobox.setItemText(1, QCoreApplication.translate("Widget", u"osu", None))
        self.modearea_combobox.setItemText(2, QCoreApplication.translate("Widget", u"custom", None))

#if QT_CONFIG(tooltip)
        self.reset_rotation_button.setToolTip(QCoreApplication.translate("Widget", u"reset rotation", None))
#endif // QT_CONFIG(tooltip)
        self.reset_rotation_button.setText(QCoreApplication.translate("Widget", u"\u21b6", None))
        self.label_22.setText(QCoreApplication.translate("Widget", u"A3", None))
        self.label_15.setText(QCoreApplication.translate("Widget", u"Area:", None))
        self.label_14.setText(QCoreApplication.translate("Widget", u"Rotation:", None))
#if QT_CONFIG(tooltip)
        self.reset_area_button.setToolTip(QCoreApplication.translate("Widget", u"reset area", None))
#endif // QT_CONFIG(tooltip)
        self.reset_area_button.setText(QCoreApplication.translate("Widget", u"\u21b6", None))
        self.label_21.setText(QCoreApplication.translate("Widget", u"A1", None))
        self.label_23.setText(QCoreApplication.translate("Widget", u"A2", None))
        self.label_24.setText(QCoreApplication.translate("Widget", u"A4", None))
        self.screen_pixmap.setText("")
        self.label_19.setText("")
        self.label_17.setText(QCoreApplication.translate("Widget", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans'; font-size:12pt;\">PanelTablet is made by Lu15ange7</span></p></body></html>", None))
        self.plainTextEdit.setPlainText(QCoreApplication.translate("Widget", u"                  GNU LESSER GENERAL PUBLIC LICENSE\n"
"                       Version 2.1, February 1999\n"
"\n"
" Copyright (C) 1991, 1999 Free Software Foundation, Inc.\n"
" 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA\n"
" Everyone is permitted to copy and distribute verbatim copies\n"
" of this license document, but changing it is not allowed.\n"
"\n"
"[This is the first released version of the Lesser GPL.  It also counts\n"
" as the successor of the GNU Library Public License, version 2, hence\n"
" the version number 2.1.]\n"
"\n"
"                            Preamble\n"
"\n"
"  The licenses for most software are designed to take away your\n"
"freedom to share and change it.  By contrast, the GNU General Public\n"
"Licenses are intended to guarantee your freedom to share and change\n"
"free software--to make sure the software is free for all its users.\n"
"\n"
"  This license, the Lesser General Public License, applies to some\n"
"specially designated software packages--typically librarie"
                        "s--of the\n"
"Free Software Foundation and other authors who decide to use it.  You\n"
"can use it too, but we suggest you first think carefully about whether\n"
"this license or the ordinary General Public License is the better\n"
"strategy to use in any particular case, based on the explanations below.\n"
"\n"
"  When we speak of free software, we are referring to freedom of use,\n"
"not price.  Our General Public Licenses are designed to make sure that\n"
"you have the freedom to distribute copies of free software (and charge\n"
"for this service if you wish); that you receive source code or can get\n"
"it if you want it; that you can change the software and use pieces of\n"
"it in new free programs; and that you are informed that you can do\n"
"these things.\n"
"\n"
"  To protect your rights, we need to make restrictions that forbid\n"
"distributors to deny you these rights or to ask you to surrender these\n"
"rights.  These restrictions translate to certain responsibilities for\n"
"you if you distribute c"
                        "opies of the library or if you modify it.\n"
"\n"
"  For example, if you distribute copies of the library, whether gratis\n"
"or for a fee, you must give the recipients all the rights that we gave\n"
"you.  You must make sure that they, too, receive or can get the source\n"
"code.  If you link other code with the library, you must provide\n"
"complete object files to the recipients, so that they can relink them\n"
"with the library after making changes to the library and recompiling\n"
"it.  And you must show them these terms so they know their rights.\n"
"\n"
"  We protect your rights with a two-step method: (1) we copyright the\n"
"library, and (2) we offer you this license, which gives you legal\n"
"permission to copy, distribute and/or modify the library.\n"
"\n"
"  To protect each distributor, we want to make it very clear that\n"
"there is no warranty for the free library.  Also, if the library is\n"
"modified by someone else and passed on, the recipients should know\n"
"that what they have is not the or"
                        "iginal version, so that the original\n"
"author's reputation will not be affected by problems that might be\n"
"introduced by others.\n"
"\n"
"  Finally, software patents pose a constant threat to the existence of\n"
"any free program.  We wish to make sure that a company cannot\n"
"effectively restrict the users of a free program by obtaining a\n"
"restrictive license from a patent holder.  Therefore, we insist that\n"
"any patent license obtained for a version of the library must be\n"
"consistent with the full freedom of use specified in this license.\n"
"\n"
"  Most GNU software, including some libraries, is covered by the\n"
"ordinary GNU General Public License.  This license, the GNU Lesser\n"
"General Public License, applies to certain designated libraries, and\n"
"is quite different from the ordinary General Public License.  We use\n"
"this license for certain libraries in order to permit linking those\n"
"libraries into non-free programs.\n"
"\n"
"  When a program is linked with a library, whether sta"
                        "tically or using\n"
"a shared library, the combination of the two is legally speaking a\n"
"combined work, a derivative of the original library.  The ordinary\n"
"General Public License therefore permits such linking only if the\n"
"entire combination fits its criteria of freedom.  The Lesser General\n"
"Public License permits more lax criteria for linking other code with\n"
"the library.\n"
"\n"
"  We call this license the \"Lesser\" General Public License because it\n"
"does Less to protect the user's freedom than the ordinary General\n"
"Public License.  It also provides other free software developers Less\n"
"of an advantage over competing non-free programs.  These disadvantages\n"
"are the reason we use the ordinary General Public License for many\n"
"libraries.  However, the Lesser license provides advantages in certain\n"
"special circumstances.\n"
"\n"
"  For example, on rare occasions, there may be a special need to\n"
"encourage the widest possible use of a certain library, so that it becomes\n"
"a d"
                        "e-facto standard.  To achieve this, non-free programs must be\n"
"allowed to use the library.  A more frequent case is that a free\n"
"library does the same job as widely used non-free libraries.  In this\n"
"case, there is little to gain by limiting the free library to free\n"
"software only, so we use the Lesser General Public License.\n"
"\n"
"  In other cases, permission to use a particular library in non-free\n"
"programs enables a greater number of people to use a large body of\n"
"free software.  For example, permission to use the GNU C Library in\n"
"non-free programs enables many more people to use the whole GNU\n"
"operating system, as well as its variant, the GNU/Linux operating\n"
"system.\n"
"\n"
"  Although the Lesser General Public License is Less protective of the\n"
"users' freedom, it does ensure that the user of a program that is\n"
"linked with the Library has the freedom and the wherewithal to run\n"
"that program using a modified version of the Library.\n"
"\n"
"  The precise terms and co"
                        "nditions for copying, distribution and\n"
"modification follow.  Pay close attention to the difference between a\n"
"\"work based on the library\" and a \"work that uses the library\".  The\n"
"former contains code derived from the library, whereas the latter must\n"
"be combined with the library in order to run.\n"
"\n"
"                  GNU LESSER GENERAL PUBLIC LICENSE\n"
"   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION\n"
"\n"
"  0. This License Agreement applies to any software library or other\n"
"program which contains a notice placed by the copyright holder or\n"
"other authorized party saying it may be distributed under the terms of\n"
"this Lesser General Public License (also called \"this License\").\n"
"Each licensee is addressed as \"you\".\n"
"\n"
"  A \"library\" means a collection of software functions and/or data\n"
"prepared so as to be conveniently linked with application programs\n"
"(which use some of those functions and data) to form executables.\n"
"\n"
"  The \"Libra"
                        "ry\", below, refers to any such software library or work\n"
"which has been distributed under these terms.  A \"work based on the\n"
"Library\" means either the Library or any derivative work under\n"
"copyright law: that is to say, a work containing the Library or a\n"
"portion of it, either verbatim or with modifications and/or translated\n"
"straightforwardly into another language.  (Hereinafter, translation is\n"
"included without limitation in the term \"modification\".)\n"
"\n"
"  \"Source code\" for a work means the preferred form of the work for\n"
"making modifications to it.  For a library, complete source code means\n"
"all the source code for all modules it contains, plus any associated\n"
"interface definition files, plus the scripts used to control compilation\n"
"and installation of the library.\n"
"\n"
"  Activities other than copying, distribution and modification are not\n"
"covered by this License; they are outside its scope.  The act of\n"
"running a program using the Library is not restric"
                        "ted, and output from\n"
"such a program is covered only if its contents constitute a work based\n"
"on the Library (independent of the use of the Library in a tool for\n"
"writing it).  Whether that is true depends on what the Library does\n"
"and what the program that uses the Library does.\n"
"\n"
"  1. You may copy and distribute verbatim copies of the Library's\n"
"complete source code as you receive it, in any medium, provided that\n"
"you conspicuously and appropriately publish on each copy an\n"
"appropriate copyright notice and disclaimer of warranty; keep intact\n"
"all the notices that refer to this License and to the absence of any\n"
"warranty; and distribute a copy of this License along with the\n"
"Library.\n"
"\n"
"  You may charge a fee for the physical act of transferring a copy,\n"
"and you may at your option offer warranty protection in exchange for a\n"
"fee.\n"
"\n"
"  2. You may modify your copy or copies of the Library or any portion\n"
"of it, thus forming a work based on the Library, a"
                        "nd copy and\n"
"distribute such modifications or work under the terms of Section 1\n"
"above, provided that you also meet all of these conditions:\n"
"\n"
"    a) The modified work must itself be a software library.\n"
"\n"
"    b) You must cause the files modified to carry prominent notices\n"
"    stating that you changed the files and the date of any change.\n"
"\n"
"    c) You must cause the whole of the work to be licensed at no\n"
"    charge to all third parties under the terms of this License.\n"
"\n"
"    d) If a facility in the modified Library refers to a function or a\n"
"    table of data to be supplied by an application program that uses\n"
"    the facility, other than as an argument passed when the facility\n"
"    is invoked, then you must make a good faith effort to ensure that,\n"
"    in the event an application does not supply such function or\n"
"    table, the facility still operates, and performs whatever part of\n"
"    its purpose remains meaningful.\n"
"\n"
"    (For example, a funct"
                        "ion in a library to compute square roots has\n"
"    a purpose that is entirely well-defined independent of the\n"
"    application.  Therefore, Subsection 2d requires that any\n"
"    application-supplied function or table used by this function must\n"
"    be optional: if the application does not supply it, the square\n"
"    root function must still compute square roots.)\n"
"\n"
"These requirements apply to the modified work as a whole.  If\n"
"identifiable sections of that work are not derived from the Library,\n"
"and can be reasonably considered independent and separate works in\n"
"themselves, then this License, and its terms, do not apply to those\n"
"sections when you distribute them as separate works.  But when you\n"
"distribute the same sections as part of a whole which is a work based\n"
"on the Library, the distribution of the whole must be on the terms of\n"
"this License, whose permissions for other licensees extend to the\n"
"entire whole, and thus to each and every part regardless of who wro"
                        "te\n"
"it.\n"
"\n"
"Thus, it is not the intent of this section to claim rights or contest\n"
"your rights to work written entirely by you; rather, the intent is to\n"
"exercise the right to control the distribution of derivative or\n"
"collective works based on the Library.\n"
"\n"
"In addition, mere aggregation of another work not based on the Library\n"
"with the Library (or with a work based on the Library) on a volume of\n"
"a storage or distribution medium does not bring the other work under\n"
"the scope of this License.\n"
"\n"
"  3. You may opt to apply the terms of the ordinary GNU General Public\n"
"License instead of this License to a given copy of the Library.  To do\n"
"this, you must alter all the notices that refer to this License, so\n"
"that they refer to the ordinary GNU General Public License, version 2,\n"
"instead of to this License.  (If a newer version than version 2 of the\n"
"ordinary GNU General Public License has appeared, then you can specify\n"
"that version instead if you wish.)  "
                        "Do not make any other change in\n"
"these notices.\n"
"\n"
"  Once this change is made in a given copy, it is irreversible for\n"
"that copy, so the ordinary GNU General Public License applies to all\n"
"subsequent copies and derivative works made from that copy.\n"
"\n"
"  This option is useful when you wish to copy part of the code of\n"
"the Library into a program that is not a library.\n"
"\n"
"  4. You may copy and distribute the Library (or a portion or\n"
"derivative of it, under Section 2) in object code or executable form\n"
"under the terms of Sections 1 and 2 above provided that you accompany\n"
"it with the complete corresponding machine-readable source code, which\n"
"must be distributed under the terms of Sections 1 and 2 above on a\n"
"medium customarily used for software interchange.\n"
"\n"
"  If distribution of object code is made by offering access to copy\n"
"from a designated place, then offering equivalent access to copy the\n"
"source code from the same place satisfies the requirement to"
                        "\n"
"distribute the source code, even though third parties are not\n"
"compelled to copy the source along with the object code.\n"
"\n"
"  5. A program that contains no derivative of any portion of the\n"
"Library, but is designed to work with the Library by being compiled or\n"
"linked with it, is called a \"work that uses the Library\".  Such a\n"
"work, in isolation, is not a derivative work of the Library, and\n"
"therefore falls outside the scope of this License.\n"
"\n"
"  However, linking a \"work that uses the Library\" with the Library\n"
"creates an executable that is a derivative of the Library (because it\n"
"contains portions of the Library), rather than a \"work that uses the\n"
"library\".  The executable is therefore covered by this License.\n"
"Section 6 states terms for distribution of such executables.\n"
"\n"
"  When a \"work that uses the Library\" uses material from a header file\n"
"that is part of the Library, the object code for the work may be a\n"
"derivative work of the Library even"
                        " though the source code is not.\n"
"Whether this is true is especially significant if the work can be\n"
"linked without the Library, or if the work is itself a library.  The\n"
"threshold for this to be true is not precisely defined by law.\n"
"\n"
"  If such an object file uses only numerical parameters, data\n"
"structure layouts and accessors, and small macros and small inline\n"
"functions (ten lines or less in length), then the use of the object\n"
"file is unrestricted, regardless of whether it is legally a derivative\n"
"work.  (Executables containing this object code plus portions of the\n"
"Library will still fall under Section 6.)\n"
"\n"
"  Otherwise, if the work is a derivative of the Library, you may\n"
"distribute the object code for the work under the terms of Section 6.\n"
"Any executables containing that work also fall under Section 6,\n"
"whether or not they are linked directly with the Library itself.\n"
"\n"
"  6. As an exception to the Sections above, you may also combine or\n"
"link a \""
                        "work that uses the Library\" with the Library to produce a\n"
"work containing portions of the Library, and distribute that work\n"
"under terms of your choice, provided that the terms permit\n"
"modification of the work for the customer's own use and reverse\n"
"engineering for debugging such modifications.\n"
"\n"
"  You must give prominent notice with each copy of the work that the\n"
"Library is used in it and that the Library and its use are covered by\n"
"this License.  You must supply a copy of this License.  If the work\n"
"during execution displays copyright notices, you must include the\n"
"copyright notice for the Library among them, as well as a reference\n"
"directing the user to the copy of this License.  Also, you must do one\n"
"of these things:\n"
"\n"
"    a) Accompany the work with the complete corresponding\n"
"    machine-readable source code for the Library including whatever\n"
"    changes were used in the work (which must be distributed under\n"
"    Sections 1 and 2 above); and, if th"
                        "e work is an executable linked\n"
"    with the Library, with the complete machine-readable \"work that\n"
"    uses the Library\", as object code and/or source code, so that the\n"
"    user can modify the Library and then relink to produce a modified\n"
"    executable containing the modified Library.  (It is understood\n"
"    that the user who changes the contents of definitions files in the\n"
"    Library will not necessarily be able to recompile the application\n"
"    to use the modified definitions.)\n"
"\n"
"    b) Use a suitable shared library mechanism for linking with the\n"
"    Library.  A suitable mechanism is one that (1) uses at run time a\n"
"    copy of the library already present on the user's computer system,\n"
"    rather than copying library functions into the executable, and (2)\n"
"    will operate properly with a modified version of the library, if\n"
"    the user installs one, as long as the modified version is\n"
"    interface-compatible with the version that the work was made w"
                        "ith.\n"
"\n"
"    c) Accompany the work with a written offer, valid for at\n"
"    least three years, to give the same user the materials\n"
"    specified in Subsection 6a, above, for a charge no more\n"
"    than the cost of performing this distribution.\n"
"\n"
"    d) If distribution of the work is made by offering access to copy\n"
"    from a designated place, offer equivalent access to copy the above\n"
"    specified materials from the same place.\n"
"\n"
"    e) Verify that the user has already received a copy of these\n"
"    materials or that you have already sent this user a copy.\n"
"\n"
"  For an executable, the required form of the \"work that uses the\n"
"Library\" must include any data and utility programs needed for\n"
"reproducing the executable from it.  However, as a special exception,\n"
"the materials to be distributed need not include anything that is\n"
"normally distributed (in either source or binary form) with the major\n"
"components (compiler, kernel, and so on) of the operating s"
                        "ystem on\n"
"which the executable runs, unless that component itself accompanies\n"
"the executable.\n"
"\n"
"  It may happen that this requirement contradicts the license\n"
"restrictions of other proprietary libraries that do not normally\n"
"accompany the operating system.  Such a contradiction means you cannot\n"
"use both them and the Library together in an executable that you\n"
"distribute.\n"
"\n"
"  7. You may place library facilities that are a work based on the\n"
"Library side-by-side in a single library together with other library\n"
"facilities not covered by this License, and distribute such a combined\n"
"library, provided that the separate distribution of the work based on\n"
"the Library and of the other library facilities is otherwise\n"
"permitted, and provided that you do these two things:\n"
"\n"
"    a) Accompany the combined library with a copy of the same work\n"
"    based on the Library, uncombined with any other library\n"
"    facilities.  This must be distributed under the terms o"
                        "f the\n"
"    Sections above.\n"
"\n"
"    b) Give prominent notice with the combined library of the fact\n"
"    that part of it is a work based on the Library, and explaining\n"
"    where to find the accompanying uncombined form of the same work.\n"
"\n"
"  8. You may not copy, modify, sublicense, link with, or distribute\n"
"the Library except as expressly provided under this License.  Any\n"
"attempt otherwise to copy, modify, sublicense, link with, or\n"
"distribute the Library is void, and will automatically terminate your\n"
"rights under this License.  However, parties who have received copies,\n"
"or rights, from you under this License will not have their licenses\n"
"terminated so long as such parties remain in full compliance.\n"
"\n"
"  9. You are not required to accept this License, since you have not\n"
"signed it.  However, nothing else grants you permission to modify or\n"
"distribute the Library or its derivative works.  These actions are\n"
"prohibited by law if you do not accept this Licens"
                        "e.  Therefore, by\n"
"modifying or distributing the Library (or any work based on the\n"
"Library), you indicate your acceptance of this License to do so, and\n"
"all its terms and conditions for copying, distributing or modifying\n"
"the Library or works based on it.\n"
"\n"
"  10. Each time you redistribute the Library (or any work based on the\n"
"Library), the recipient automatically receives a license from the\n"
"original licensor to copy, distribute, link with or modify the Library\n"
"subject to these terms and conditions.  You may not impose any further\n"
"restrictions on the recipients' exercise of the rights granted herein.\n"
"You are not responsible for enforcing compliance by third parties with\n"
"this License.\n"
"\n"
"  11. If, as a consequence of a court judgment or allegation of patent\n"
"infringement or for any other reason (not limited to patent issues),\n"
"conditions are imposed on you (whether by court order, agreement or\n"
"otherwise) that contradict the conditions of this License, "
                        "they do not\n"
"excuse you from the conditions of this License.  If you cannot\n"
"distribute so as to satisfy simultaneously your obligations under this\n"
"License and any other pertinent obligations, then as a consequence you\n"
"may not distribute the Library at all.  For example, if a patent\n"
"license would not permit royalty-free redistribution of the Library by\n"
"all those who receive copies directly or indirectly through you, then\n"
"the only way you could satisfy both it and this License would be to\n"
"refrain entirely from distribution of the Library.\n"
"\n"
"If any portion of this section is held invalid or unenforceable under any\n"
"particular circumstance, the balance of the section is intended to apply,\n"
"and the section as a whole is intended to apply in other circumstances.\n"
"\n"
"It is not the purpose of this section to induce you to infringe any\n"
"patents or other property right claims or to contest validity of any\n"
"such claims; this section has the sole purpose of protecting"
                        " the\n"
"integrity of the free software distribution system which is\n"
"implemented by public license practices.  Many people have made\n"
"generous contributions to the wide range of software distributed\n"
"through that system in reliance on consistent application of that\n"
"system; it is up to the author/donor to decide if he or she is willing\n"
"to distribute software through any other system and a licensee cannot\n"
"impose that choice.\n"
"\n"
"This section is intended to make thoroughly clear what is believed to\n"
"be a consequence of the rest of this License.\n"
"\n"
"  12. If the distribution and/or use of the Library is restricted in\n"
"certain countries either by patents or by copyrighted interfaces, the\n"
"original copyright holder who places the Library under this License may add\n"
"an explicit geographical distribution limitation excluding those countries,\n"
"so that distribution is permitted only in or among countries not thus\n"
"excluded.  In such case, this License incorporates the li"
                        "mitation as if\n"
"written in the body of this License.\n"
"\n"
"  13. The Free Software Foundation may publish revised and/or new\n"
"versions of the Lesser General Public License from time to time.\n"
"Such new versions will be similar in spirit to the present version,\n"
"but may differ in detail to address new problems or concerns.\n"
"\n"
"Each version is given a distinguishing version number.  If the Library\n"
"specifies a version number of this License which applies to it and\n"
"\"any later version\", you have the option of following the terms and\n"
"conditions either of that version or of any later version published by\n"
"the Free Software Foundation.  If the Library does not specify a\n"
"license version number, you may choose any version ever published by\n"
"the Free Software Foundation.\n"
"\n"
"  14. If you wish to incorporate parts of the Library into other free\n"
"programs whose distribution conditions are incompatible with these,\n"
"write to the author to ask for permission.  For software"
                        " which is\n"
"copyrighted by the Free Software Foundation, write to the Free\n"
"Software Foundation; we sometimes make exceptions for this.  Our\n"
"decision will be guided by the two goals of preserving the free status\n"
"of all derivatives of our free software and of promoting the sharing\n"
"and reuse of software generally.\n"
"\n"
"                            NO WARRANTY\n"
"\n"
"  15. BECAUSE THE LIBRARY IS LICENSED FREE OF CHARGE, THERE IS NO\n"
"WARRANTY FOR THE LIBRARY, TO THE EXTENT PERMITTED BY APPLICABLE LAW.\n"
"EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR\n"
"OTHER PARTIES PROVIDE THE LIBRARY \"AS IS\" WITHOUT WARRANTY OF ANY\n"
"KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE\n"
"IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR\n"
"PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE\n"
"LIBRARY IS WITH YOU.  SHOULD THE LIBRARY PROVE DEFECTIVE, YOU ASSUME\n"
"THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.\n"
""
                        "\n"
"  16. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN\n"
"WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY\n"
"AND/OR REDISTRIBUTE THE LIBRARY AS PERMITTED ABOVE, BE LIABLE TO YOU\n"
"FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR\n"
"CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE\n"
"LIBRARY (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING\n"
"RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A\n"
"FAILURE OF THE LIBRARY TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF\n"
"SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH\n"
"DAMAGES.\n"
"\n"
"                     END OF TERMS AND CONDITIONS\n"
"\n"
"           How to Apply These Terms to Your New Libraries\n"
"\n"
"  If you develop a new library, and you want it to be of the greatest\n"
"possible use to the public, we recommend making it free software that\n"
"everyone can redistribute and change.  You can do so by permitting\n"
"redistribution "
                        "under these terms (or, alternatively, under the terms of the\n"
"ordinary General Public License).\n"
"\n"
"  To apply these terms, attach the following notices to the library.  It is\n"
"safest to attach them to the start of each source file to most effectively\n"
"convey the exclusion of warranty; and each file should have at least the\n"
"\"copyright\" line and a pointer to where the full notice is found.\n"
"\n"
"    A610PRODRIVER  Copyright (C) 2024 Lu15ange7\n"
"\n"
"    This library is free software; you can redistribute it and/or\n"
"    modify it under the terms of the GNU Lesser General Public\n"
"    License as published by the Free Software Foundation; either\n"
"    version 2.1 of the License, or (at your option) any later version.\n"
"\n"
"    This library is distributed in the hope that it will be useful,\n"
"    but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n"
"    Lesser General Public License for more d"
                        "etails.\n"
"\n"
"    You should have received a copy of the GNU Lesser General Public\n"
"    License along with this library; if not, write to the Free Software\n"
"    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301\n"
"    USA\n"
"\n"
"Also add information on how to contact you by electronic and paper mail.\n"
"\n"
"You should also get your employer (if you work as a programmer) or your\n"
"school, if any, to sign a \"copyright disclaimer\" for the library, if\n"
"necessary.  Here is a sample; alter the names:\n"
"\n"
"  Yoyodyne, Inc., hereby disclaims all copyright interest in the\n"
"  library `Frob' (a library for tweaking knobs) written by James Random\n"
"  Hacker.\n"
"\n"
"  <signature of Ty Coon>, 1 April 1990\n"
"  Ty Coon, President of Vice\n"
"\n"
"That's all there is to it!\n"
"", None))
#if QT_CONFIG(tooltip)
        self.apply_button.setToolTip(QCoreApplication.translate("Widget", u"apply changes", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.apply_button.setStatusTip("")
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        self.apply_button.setWhatsThis("")
#endif // QT_CONFIG(whatsthis)
        self.apply_button.setText(QCoreApplication.translate("Widget", u"Apply", None))
#if QT_CONFIG(tooltip)
        self.reset_all_button.setToolTip(QCoreApplication.translate("Widget", u"reset changes", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        self.reset_all_button.setStatusTip("")
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        self.reset_all_button.setWhatsThis("")
#endif // QT_CONFIG(whatsthis)
        self.reset_all_button.setText(QCoreApplication.translate("Widget", u"Reset", None))
#if QT_CONFIG(tooltip)
        self.exit_button.setToolTip(QCoreApplication.translate("Widget", u"exit", None))
#endif // QT_CONFIG(tooltip)
        self.exit_button.setText(QCoreApplication.translate("Widget", u"Exit", None))
    # retranslateUi

