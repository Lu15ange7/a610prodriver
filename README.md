# A610ProDriver
A unofficial Parblo A610 Pro driver support for GNU/Linux. Scripts made by [lu15ange7](https://gitlab.com/Lu15ange7 "Lu15ange7").
```			
         d8888  .d8888b.   d888    .d8888b.  8888888b.                   
        d88888 d88P  Y88b d8888   d88P  Y88b 888   Y88b                  
       d88P888 888          888   888    888 888    888                  
      d88P 888 888d888b.    888   888    888 888   d88P 888d888  .d88b.  
     d88P  888 888P "Y88b   888   888    888 8888888P"  888P"   d88""88b 
    d88P   888 888    888   888   888    888 888        888     888  888
   d8888888888 Y88b  d88P   888   Y88b  d88P 888        888     Y88..88P 
  d88P     888  "Y8888P"  8888888  "Y8888P"  888        888      "Y88P"  

```

WARNING:  For use the wheel in XORG, just press K3 and K4 buttoms many times and try if work. THE WHEEL NOT WORK WITH WAYLAND FOR A CONFLICT WITH LIBINPUT AND LIBWACOM (I will try fix soon for both cases).

THIS PRODUCT IS NOT WARRANTY, PLEASE READ [LICENSE](LICENSE "LICENSE") BEFORE USE.

![Parblo A610 Pro](preview.png "Panel Tablet, a GUI for Xorg Desktop Enviroment")

## Requirements
You need to have these dependencies installed (the packagse varies depending on the linux distribution you use, I take for example some in the case of Arch Linux / Manjaro /  Artix).

Dependencies for minimal version: `usbutils` `xorg-xinput` `xf86-input-wacom` `xf86-input-libinput` `xf86-input-evdev` `libevdev` `libwacom` `libinput`.

Dependencies for GUI version: same as minimal version and `pyside6`

Dependencies for minimal version: same as minimal version and GUI version.

## Install

### Install with makefile (Any Distro)

Open the "driver" directory and run this script in a terminal.

- Use `install_driver` for install the essential files for Xorg or Wayland Desktop Enviroment like GNOME or KDE using the default panel.
```
make install_driver
```

- Use `install_gui` for install the GUI for Xorg Desktop Enviroment like XFCE, LXQT, LXDE, etc.

```
make install_gui
```

- Use `install_full` for install the full version, with endev rules and others config scripts (use only for develop or testing)

```
make install_full
```

Reboot the system.

```
reboot
```

### Install from my own AUR (For Arch Linux / Manjaro / Artix)

For install on ArchLinux, Manjaro, Artix, etc. you need install from my repository [hicat-aur](https://gitlab.com/Lu15ange7/hitcat-aur "hitcat-aur"), and update pacman database. using `sudo pacman -Syu`.

Run the commnad for install the packeage from pacman, remember select the options `a610prodriver`, `a610prodriver-gui` or `a610prodriver-full` for install the driver.

- `a610prodriver`: Minimal driver version
```
sudo pacman -S a610prodriver
```
- `a610prodriver-gui`: GUI for A610prodriver
```
sudo pacman -S a610prodriver-gui
```
- `a610prodriver-full`: Full version only for develop or testing
```
sudo pacman -S a610prodriver-full
```

## Check

When your computer is turned on, you can check that the driver is installed correctly cheking `lsusb` and `libinput debug-events` (Xorg and Wayland).

```
$ lsusb
Bus 001 Device 003: ID 28bd:1903 XP-Pen 10 inch PenTablet
```
```
$ libinput debug-events
-event5   DEVICE_ADDED            10 inch PenTablet Mouse           seat0 default group7  cap:p left scroll-nat scroll-button
-event6   DEVICE_ADDED            10 inch PenTablet Pen             seat0 default group7  cap:T  size 255x160mm
-event7   DEVICE_ADDED            10 inch PenTablet Pad             seat0 default group7  cap:P buttons:9 strips:0 rings:0 mode groups:1
```
Check `xinput` and `xsetwacom` (only Xorg).

```
$ xinput list
⎜   ↳ 10 inch PenTablet Pad pad               	id=15	[slave  pointer  (2)]
⎜   ↳ 10 inch PenTablet Pen stylus            	id=16	[slave  pointer  (2)]
```

```
$ xsetwacom list
10 inch PenTablet Pad pad       	id: 15	type: PAD
10 inch PenTablet Pen stylus    	id: 16	type: STYLUS 
```

## Config with Wayland (Libinput/Libwacom)

Wayland don't can use xsetwacom, so you need to use libinput, for change the map of buttons you need use GNOME or KDE panel control for change the map of buttons and others options.


## Config with X11/Xorg (XInput/XSetWacom)

You can use 3 options for config it:

- Use the PanelTablet GUI (`/opt/a610prodriver/paneltablet` only for full version), just is a GUI automatized for config the buttons, pen and tablet area, use it only on X11/Xorg Desktop Enviroment like XFCE, LXQT, LXDE, etc.

- Use the GNOME or KDE tablet config from the panel, just is a GUI automatized for config the buttons, pen and tablet area, for load its config, use the comand `a610prodriver` option `1`.

- Use a script like `/opt/a610prodriver/default` for set the buttons, pen and tablet area, for load its config, use the comand `a610prodriver` option `2` for set to Default config, but if you want make you own config, you can copy the `default` file to user directory and add an alias on you `.bashrc` or `.zshrc` for load it automatically. 

    Example: 
    ```
    alias a610prodriver-custom="sh ~/path/myowmprodile"
    ```


## Extra

The `a610pro_krita.shortcuts` file contains keyboard shortcuts for Krita, you just have to import it to Krita, select the information added to "Default" and apply the changes, here it shows the procedure in the [Manual](https://docs.krita.org/en/reference_manual/preferences/shortcut_settings.html).


## Uninstall

### Uninstall with makefile (Any Distro)

Run this script for remove the driver and its files, dont worry if on the log show some not found files, just ignore it.

```
make uninstall
```

Reboot the system for load the changes.

```
reboot
```

### Uninstall for Arch Linux / Manjaro / Artix

Run the commnad for uninstall the packeage from pacman, remember select the options `a610prodriver`, `a610prodriver-gui` or `a610prodriver-full` for uninstall the driver.

- `a610prodriver`: Minimal version
```
sudo pacman -Rs a610prodriver
```
- `a610prodriver-gui`: GUI for A610prodriver
```
sudo pacman -Rs a610prodriver-gui
```
- `a610prodriver-full`: Full version only for develop or testing
```
sudo pacman -Rs a610prodriver-full
```

Reboot the system for load the changes.

```
reboot
```

Thanks you and good luck.

