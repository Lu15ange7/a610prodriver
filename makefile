all: welcome

welcome:
	@echo '										'
	@echo '-------------------------------------------------------------------------'
	@echo '										'
	@echo '         d8888  .d8888b.   d888    .d8888b.  8888888b.                   '
	@echo '        d88888 d88P  Y88b d8888   d88P  Y88b 888   Y88b                  '
	@echo '       d88P888 888          888   888    888 888    888                  '
	@echo '      d88P 888 888d888b.    888   888    888 888   d88P 888d888  .d88b.  '
	@echo '     d88P  888 888P "Y88b   888   888    888 8888888P"  888P"   d88""88b '
	@echo '    d88P   888 888    888   888   888    888 888        888     888  888 '
	@echo '   d8888888888 Y88b  d88P   888   Y88b  d88P 888        888     Y88..88P '
	@echo '  d88P     888  "Y8888P"  8888888  "Y8888P"  888        888      "Y88P"  '
	@echo '										'
	@echo '		 A unofficial Parblo A610 Pro driver support for GNU/Linux.'
	@echo '										'
	@echo '-------------------------------------------------------------------------'
	@echo '										'
	@sleep 0.5


install_full: welcome
	@echo " ✓ A610ProDriver Installing Driver Full"
	@echo '										'
	@echo " ✓ WARNING: Only for develop or testing"
	@sudo cp driver/66-libwacom.hwdb /etc/udev/hwdb.d/
	@sudo cp driver/a610pro.tablet /usr/share/libwacom/
	@sudo cp driver/99-tablet.rules /etc/udev/rules.d/
	@sudo cp driver/50-A610PRO.conf /etc/X11/xorg.conf.d/
	@sudo cp driver/A610PRO.conf /etc/libinput
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver Installing Profiles"
	@chmod +x driver/default
	@chmod +x driver/a610prodriver
	@sudo mkdir /opt/a610pro
	@sudo cp driver/paneltablet_config.ini /opt/a610pro/
	@sudo cp driver/default /opt/a610pro/
	@sudo cp driver/tabletdblocalrc /opt/a610pro/
	@sudo cp driver/tabletprofilesrc /opt/a610pro/
	@sudo cp driver/a610prodriver /usr/bin/
	@sudo cp driver/a610pro_krita.shortcuts /opt/a610pro/
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver Installing GUI"
	@sudo cp driver/paneltablet.desktop /usr/share/applications/
	@sudo cp GUI/img/paneltablet.svg /opt/a610pro/
	@sudo cp GUI/img_rc.py /opt/a610pro/
	@sudo cp GUI/paneltablet.py /opt/a610pro/
	@sudo cp GUI/ui_form.py /opt/a610pro/
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver installed"
	@echo '										'
	@echo " ✓ Now reboot the system for apply the changes"
	@echo '										'
	@echo '-------------------------------------------------------------------------'
	@echo '										'
								'
install_driver: welcome
	@echo " ✓ A610ProDriver Installing Driver"
	@chmod +x driver/default
	@chmod +x driver/a610prodriver
	@sudo cp driver/a610pro.tablet /usr/share/libwacom/
	@sudo cp driver/50-A610PRO.conf /etc/X11/xorg.conf.d/
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver Installing Profiles"
	@sudo mkdir /opt/a610pro
	@sudo cp driver/a610pro_krita.shortcuts /opt/a610pro/
	@sudo cp driver/tabletdblocalrc /opt/a610pro/
	@sudo cp driver/tabletprofilesrc /opt/a610pro/
	@sudo cp driver/default /opt/a610pro/
	@sudo cp driver/a610prodriver /usr/bin/
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver installed"
	@echo '										'
	@echo " ✓ Now reboot the system for apply the changes"
	@echo '										'
	@echo '-------------------------------------------------------------------------'
	@echo '										'

install_gui: welcome
	@echo " ✓ A610ProDriver Installing GUI (ONLY WORK FOR X11/XORG)"
	@sudo cp driver/paneltablet.desktop /usr/share/applications/
	@sudo cp GUI/img/paneltablet.svg /opt/a610pro/
	@sudo cp GUI/img_rc.py /opt/a610pro/
	@sudo cp GUI/paneltablet.py /opt/a610pro/
	@sudo cp GUI/ui_form.py /opt/a610pro/
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver GUI installed"
	@echo '										'
	@echo " ✓ Now reboot the system for apply the changes"
	@echo '										'
	@echo '-------------------------------------------------------------------------'
	@echo '										'

uninstall_all: welcome
	@echo " ✓ A610ProDriver Uninstalling All"
	@sudo rm -r /opt/a610pro/
	@sudo rm -f /etc/udev/hwdb.d/66-libwacom.hwdb
	@sudo rm -f /usr/share/libwacom/a610pro.tablet
	@sudo rm -f /etc/udev/rules.d/99-tablet.rules
	@sudo rm -f /etc/X11/xorg.conf.d/50-A610PRO.conf
	@sudo rm -f /etc/libinput/A610PRO.conf
	@sudo rm -f /usr/share/applications/paneltablet.desktop
	@sudo rm -f /usr/bin/a610prodriver
	@rm -r ~/.config/paneltablet/
	@sleep 0.25
	@echo '										'
	@echo " ✓ A610ProDriver uninstalled"
	@echo '										'
	@echo " ✓ Now reboot the system for apply the changes"
	@echo '										'
	@echo '-------------------------------------------------------------------------'
	@echo '										'